module.exports = (app) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  function checkUsername(val) {
    return val.length > 3;
  }


  function checkPassword(val) {
    return val.length > 5;
  }

  const UserSchema = new Schema({
    username: {
      type: String,
      required: true,
      unique: true,
      validate: checkUsername,
    },
    password: {
      required: true,
      type: String,
      validate: checkPassword,

    },
    nickName: {
      type: String,
      unique: true,
    },
    avatar: {
      type: String,
      default: "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif",
    },
    status: {
      type: Number,
      default: 1,
    },
    phone: {
      type: String,
      unique: true,
    },

    deleted: {
      type: Number,
      default: 0,
    },

    createTime: {
      type: Date,
      default: Date.now,
    },
  });
  return mongoose.model("User", UserSchema, "user");
};
