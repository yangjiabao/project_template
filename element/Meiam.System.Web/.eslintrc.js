module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  extends: ['plugin:vue/recommended', 'eslint:recommended'],

  // add your custom rules here
  //it is base on https://github.com/vuejs/eslint-config-vue
  rules: {
    "vue/max-attributes-per-line": [2, {
      "singleline": 10,
      // "multiline": {
      //   "max": 0,
      //   "allowFirstLine": false
      // }
    }],
    "vue/singleline-html-element-content-newline": "off",
    "vue/multiline-html-element-content-newline": "off",
    "vue/name-property-casing": ["error", "PascalCase"],
    "vue/no-v-html": "off",
    'accessor-pairs': 2, //强制getter/setter成对出现在对象中
    'arrow-spacing': [2, {
      'before': true,
      'after': true 
    }],  //要求箭头函数的箭头之前或之后有空格
    'block-spacing': [2, 'always'], //禁止或强制在代码块中开括号前和闭括号后有空格
    'brace-style': [2, '1tbs', {
      'allowSingleLine': true
    }],  ///大括号风格要求
    'comma-dangle': [2, 'never'], //要求或禁止使用拖尾逗号
    'comma-spacing': [2, {
      'before': false,
      'after': true
    }],   //强制在逗号周围使用空格
    'comma-style': [2, 'last'],  //逗号风格
    'curly': ["error", "multi-line"],  //要求遵循大括号约定
    'eqeqeq': ["error", "smart"],  //要求使用 === 和 !==
    'generator-star-spacing': ["error", {"before": true, "after": false}], //强制 generator 函数中 * 号周围有空格
    'handle-callback-err': [2, '^(err|error)$'],  //强制回调错误处理
    'indent': [2, 2, {
      'SwitchCase': 1
    }],
    'jsx-quotes': [2, 'prefer-single'], //强制在 JSX 属性中使用一致的单引号或双引号
    'key-spacing': 2,  //强制在对象字面量的键和值之间使用一致的空格
    'keyword-spacing': "error", //强制关键字周围空格的一致性
    'new-cap': 0,  //要求构造函数首字母大写
    'new-parens':2, //要求调用无参构造函数时带括号
		'no-caller': "error", //禁用 caller 或 callee
    'no-console':0,
    'no-eval': 2,  //禁用 eval()
    'no-extend-native': 2, //禁止扩展原生对象
    'no-extra-parens': "error", //禁止冗余的括号
    'no-floating-decimal': 2, //禁止浮点小数
    'no-implied-eval': 2,   //禁用隐式的eval()
    'no-iterator': 2,   //禁用迭代器
    'no-label-var': 2, //禁用与变量同名的标签
    'no-labels': "error", //禁用标签语句
    'no-lone-blocks': 2, //禁用不必要的嵌套块
    'no-multi-spaces': 2, //禁止出现多个空格
    'no-multi-str': 2, //禁止多行字符串
    'no-multiple-empty-lines': [2, {
      'max': 1
    }],  //不允许多个空行
    'no-new-object': 2, //禁止使用 Object 构造函数
    'no-new-require': 2, //不允许 new require
    'no-new-wrappers': 2, //禁止原始包装实例
    'no-octal': 2, //禁用八进制字面量
    'no-octal-escape': 2, //禁止在字符串字面量中使用八进制转义序列
    'no-path-concat': 2, //当使用 _dirname 和 _filename 时不允许字符串拼接
    'no-proto': 2, //禁用__proto__
    'no-return-assign': [2, 'except-parens'], //禁止在返回语句中赋值 
    'no-self-compare': 2, //禁止自身比较
    'no-sequences': 2, //不允许使用逗号操作符
    'func-call-spacing': ["error", "never"], //要求或禁止在函数标识符和其调用之间有空格
    'no-throw-literal': 2,  //限制可以被抛出的异常
    'no-unneeded-ternary': 2,  //禁止可以表达为更简单结构的三元操作符
    'no-useless-computed-key': 2,
    'no-whitespace-before-property': 2,
    'one-var': [2, {
      'initialized': 'never'
    }],  //强制函数中的变量在一起声明或分开声明
    'operator-linebreak': [2, 'after', {
      'overrides': {
        '?': 'before',
        ':': 'before'
      }
    }],  //强制操作符使用一致的换行符风格
    'padded-blocks': [2, 'never'],  //要求或禁止块内填充
    'quotes': [2, 'single', {
      'avoidEscape': true,
      'allowTemplateLiterals': true
    }], //强制使用一致的反勾号、双引号或单引号
    'semi': [2, 'never'], //要求或禁止使用分号代替 ASI
    'semi-spacing': [2, {
      'before': false,
      'after': true
    }], //强制分号前后有空格
    'space-before-blocks': [2, 'always'],  //要求或禁止语句块之前的空格
    'space-before-function-paren': [2, 'never'], //要求或禁止函数圆括号之前有一个空格
    'space-in-parens': [2, 'never'],  //禁止或强制圆括号内的空格
    "space-infix-ops": ["error", {
			"int32Hint": false
		}],  //要求中缀操作符周围有空格
    'space-unary-ops': [2, {
      'words': true,
      'nonwords': false
    }],  //求或禁止在一元操作符之前或之后存在空格
    'template-curly-spacing': [2, 'never'], //强制模板字符串中空格的使用
    'wrap-iife': ["error", "outside"], //需要把立即执行的函数包裹起来
    'yield-star-spacing': ["error", "before"], //强制在 yield* 表达式中 * 周围使用空格 
    'object-curly-spacing': [2, 'never'], //强制在花括号中使用一致的空格
    'array-bracket-spacing': [2, 'never'], //禁止或强制在括号内使用空格
    'vue/require-prop-types':0  //在props中需要类型定义

  }
}