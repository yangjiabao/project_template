const Service = require("egg").Service;

class MenuService extends Service {
  // 将菜单格式化为树结构
  async formatMenu(rolesMenuDoc) {
    let data = [];
    for (let item of rolesMenuDoc) {
      for (let each of rolesMenuDoc) {
        if (item._id.equals(each.parent_id)) {
          if (item.children) {
            item.children.push(each);
          } else {
            item.children = [each];
          }
        }
      }
      if (item.parent_id === "-1") {
        data.push(item);
      }
    }
    return data;
  }

  // 获取菜单列表
  async show() {
    const { ctx, service } = this;
    let menuDocs = await ctx.model.Menu.find().lean();
    let data = await service.menu.formatMenu(menuDocs);
    return data;
  }

  // 创建菜单
  async create(req) {
    const { ctx } = this;

    let menuDocs = await ctx.model.Menu.find({
      component: req.component,
      parent_id: { $ne: "-1" },
    });

    if (menuDocs.length === 0) {
      await ctx.model.Menu.insertMany(req);
    } else {
      ctx.helper.fail("组件名已存在");
    }
  }

  // 删除菜单
  async delete(req) {
    const { ctx } = this;

    // $or传入数组,或操作

    // let childMenu = await ctx.model.Menu.find({ parent_id: { $in: req._id } });

    // await ctx.model.Menu.deleteMany({
    //   $or: [{ _id: { $in: req._id } }, { parent_id: { $in: req._id } }],
    // });

    await ctx.model.Menu.findByIdAndDelete(req._id);

    // await ctx.model.Permission.deleteMany({
    //   menu_id: { $in: req._id.concat(childMenu) },
    // });

    await ctx.model.Permission.deleteOne({ menu_id: req._id });

    // await ctx.model.RolesPermission.deleteMany({
    //   menu_id: { $in: req._id.concat(childMenu) },
    // });

    await ctx.model.RolesPermission.deleteOne({ menu_id: req._id });
  }

  // 更新菜单
  async update(req) {
    const { ctx } = this;

    let menuDocs = await ctx.model.Menu.find({
      component: req.component,
      parent_id: { $ne: "-1" },
    });

    if (menuDocs.length > 2) {
      ctx.helper.fail("组件名已存在");
    }
    await ctx.model.Menu.findByIdAndUpdate(req._id, req);

    // if (menuDocs.length < 2) {
    //   await ctx.model.Menu.findByIdAndUpdate(req._id, req);
    // } else {
    //   ctx.helper.fail("组件名已存在");
    // }
  }

  // 获取角色权限中菜单列表
  async getRolesMenu(req) {
    const { ctx } = this;
    let data = await ctx.model.RolesPermission.find(req).distinct("menu_id");
    return data;
  }

  // 获取路由菜单
  async getRouterMenu() {
    const { ctx, service } = this;

    let roles_id = ctx.decode.roles;
    let username = ctx.decode.username;

    let menu_id = await ctx.model.RolesPermission.find({
      roles_id: { $in: roles_id },
    }).distinct("menu_id");

    let permission_id = await ctx.model.RolesPermission.find({
      roles_id: { $in: roles_id },
    }).distinct("permission_id");

    let permissionDocs = await ctx.model.Permission.find({
      _id: { $in: permission_id },
    });

    // distinct返回去重后相应字段值组成数组
    // let menu_id = await ctx.model.RolesPermission.find({ roles_id }).distinct("menu_id");
    let rolesMenuDocs = [];

    for (let item of menu_id) {
      let menuDocs = await ctx.model.Menu.findOne(
        { _id: item },
        "-sort"
      ).lean();

      let menuPermission = await ctx.model.RolesPermission.find({
        menu_id: item,
        roles_id: { $in: roles_id },
      }).distinct("permission_id");

      let dictionary_id = await ctx.model.Permission.find({
        _id: { $in: menuPermission },
      }).distinct("dictionary_id");

      let dictionaryName = await ctx.model.Dictionary.find({
        _id: { $in: dictionary_id },
      }).distinct("name");

      menuDocs.permission = dictionaryName;
      rolesMenuDocs.push(menuDocs);
      await service.menu.findParentMenu(menuDocs, rolesMenuDocs);
    }

    let data = await service.menu.formatMenu(rolesMenuDocs);
    await ctx.model.LoginUser.updateOne(
      { "user.username": username },
      { permission: permissionDocs }
    );
    return data;
  }

  // 查找父级菜单
  async findParentMenu(child, menu) {
    const { ctx, service } = this;
    let parentMenu = "";
    if (child.parent_id !== "-1") {
      parentMenu = await ctx.model.Menu.findOne({
        _id: child.parent_id,
      }).lean();
      let isRepeat = menu.find((item) => parentMenu._id.equals(item._id));
      if (!isRepeat) {
        menu.push(parentMenu);
      }
      await service.menu.findParentMenu(parentMenu, menu);
    }
  }
}

module.exports = MenuService;
