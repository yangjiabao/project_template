"use strict";

module.exports = {
  getDictionary: {
    id: { type: "string" },
    describe: { type: "string" },
    name: { type: "string" },
    createTime: { type: "string" },
  },
};
