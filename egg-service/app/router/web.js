module.exports = (app) => {
  const { router, controller } = app;

  // 获取文章列表
  router.get("/web/article", controller.web.article.show);
  router.post("/web/article", controller.web.article.create);
  router.delete("/web/article", controller.web.article.delete);
  router.put("/web/article", controller.web.article.update);
  router.get("/web/article/:_id", controller.web.article.detail);

  router.get("/web/page", controller.web.page.show);
  router.post("/web/page", controller.web.page.create);
  router.delete("/web/page", controller.web.page.delete);
  router.put("/web/page", controller.web.page.update);


  router.get("/web/menu", controller.web.menu.show);
  router.post("/web/menu", controller.web.menu.create);
  router.delete("/web/menu", controller.web.menu.delete);
  router.put("/web/menu", controller.web.menu.update);





};
