module.exports = (app) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const RolesSchema = new Schema({
    name: {
      unique:true,

      required: true,
      type: String,
    },
    describe: {
      type: String,
    },
    status: {
      type: Number,
      default:1
    },

    deleted: {
      type: Number,
      default:0
    },

    createTime: {
      type: Date,
      default: Date.now,
    },
  });
  return mongoose.model("Roles", RolesSchema, "roles");
};
