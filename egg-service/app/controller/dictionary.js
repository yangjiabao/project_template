const Controller = require("egg").Controller;
/**
 * @controller dictionary
 */
class DictionaryController extends Controller {
  // 获取字典

  // @Request {Position} {Type} {Authorization} {Description}
  // @Response {HttpStatus} {Type} {Description}

  /**
   * @Request header getDictionary Authorization
   * @Summary 获取权限字典列表
   * @router get /dictionary
   * @Response 200 getDictionary 获取成功
   */

  async show() {
    const { ctx, service } = this;

    // await ctx.helper.authorize("dictionary_show");
    let req = ctx.query;
    let res = await service.dictionary.show(req);
    ctx.helper.success({ res });
  }

  /**
   * @Request header getDictionary Authorization
   * @Summary 创建字典
   * @router post /dictionary
   * @Response 200 getDictionary 获取成功
   */

  // 创建字典
  async create() {
    const { ctx, service } = this;
    await ctx.helper.authorize("dictionary_create");

    let req = ctx.request.body;
    await service.dictionary.create(req);
    ctx.helper.success();
  }

  /**
   * @Request header getDictionary Authorization
   * @Summary 更新字典
   * @router put /dictionary
   * @Response 200 getDictionary 获取成功
   */
  // 更新字典
  async update() {
    const { ctx, service } = this;
    await ctx.helper.authorize("dictionary_update");

    let req = ctx.request.body;
    await service.dictionary.update(req);
    ctx.helper.success();
  }

  /**
   * @request header getDictionary Authorization
   * @Summary 删除字典
   * @Router delete /dictionary
   * @Response 200 getDictionary 获取成功
   */
  // 删除字典
  async delete() {
    const { ctx, service } = this;

    await ctx.helper.authorize("dictionary_delete");

    // 校验传参为数组
    ctx.validate({ _id: "array" });

    let req = ctx.request.body;
    await service.dictionary.delete(req);
    ctx.helper.success();
  }
}
module.exports = DictionaryController;
