// 这里的vm，就是我们在vue文件里面的this，所以我们能在这里获取vuex的变量，比如存放在里面的token变量
const install = (Vue, vm) => {
  // 此为自定义配置参数，具体参数见上方说明
  Vue.prototype.$u.http.setConfig({
    baseUrl: 'https://wsl.sino2sino.com:51',
    loadingText: '努力加载中',
    loadingTime: 800,
    originalData:true

  })

  // 请求拦截，配置Token等参数
  Vue.prototype.$u.http.interceptor.request = (config) => {
	  console.log(vm.vuex_token, 456)
	  
    // 引用token
    // 方式一，存放在vuex的token，假设使用了uView封装的vuex方式
    // 见：https://uviewui.com/components/globalVariable.html
	
    config.header.SYSTOKEN = vm.vuex_token

    // 可以对某个url进行特别处理，此url参数为this.$u.get(url)中的url值
    // if (config.url === '/user/login') config.header.noToken = true
    // 最后需要将config进行return
    return config
    // 如果return一个false值，则会取消本次请求
    // if(config.url == '/user/rest') return false; // 取消某次请求
  }

  // 响应拦截，判断状态码是否通过
  Vue.prototype.$u.http.interceptor.response = (res) => {
    if (res.statusCode === 200) {
      // res为服务端返回值，可能有code，result等字段
      // 这里对res.result进行返回，将会在this.$u.post(url).then(res => {})的then回调中的res的到
      // 如果配置了originalData为true，请留意这里的返回值
      return res
    } else if (res.statusCode === 401) {
		   // vm.$u.toast(res.message)
		   
		   window.location.reload()

      // 假设401为token失效，这里跳转登录
	  
      // vm.$u.toast(res.description)
      // setTimeout(() => {
      // 	// 此为uView的方法，详见路由相关文档
      // 	vm.$u.route('/pages/user/login')
      // }, 1500)
      return false
    } else {
      vm.$u.toast(res.message)
      // 如果返回false，则会调用Promise的reject回调，
      // 并将进入this.$u.post(url).then().catch(res=>{})的catch回调中，res为服务端的返回值

      return false
    }
  }
}

export default {
  install
}
