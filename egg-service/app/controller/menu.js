const menu = require("../model/menu");

const Controller = require("egg").Controller;
class MenuController extends Controller {
  // 获取菜单
  async show() {
    const { ctx, service } = this;
    // await ctx.helper.authorize("menu_show");
    let res = await service.menu.show();
    ctx.helper.success({ res });
  }

  // 创建菜单
  async create() {
    const { ctx, service } = this;
    await ctx.helper.authorize("menu_create");

    let req = ctx.request.body;
    await service.menu.create(req);
    ctx.helper.success();
  }

  // 删除菜单
  async delete() {
    const { ctx, service } = this;
    await ctx.helper.authorize("menu_delete");
    // 校验传参为数组
    // ctx.validate({ _id: "array" });
    let req = ctx.request.body;
    await service.menu.delete(req);
    ctx.helper.success();
  }

  // 更新菜单
  async update() {
    const { ctx, service } = this;
    await ctx.helper.authorize("menu_update");
    let req = ctx.request.body;
    await service.menu.update(req);

    ctx.helper.success();
  }

  // 获取路由菜单
  async getRouterMenu() {
    const { ctx, service } = this;
    let res = await service.menu.getRouterMenu();
    ctx.helper.success({ res });
  }

  // 获取角色权限中菜单列表
  async getRolesMenu() {
    const { ctx, service } = this;
    let req = ctx.query;
    let res = await service.menu.getRolesMenu(req);
    ctx.helper.success({ res });
  }
}
module.exports = MenuController;
