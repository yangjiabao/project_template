"use strict";

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = (appInfo) => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = (exports = {});

  config.cluster = {
    listen: {
      path: "",
      port: 7002,
      hostname: "127.0.0.1",
    },
  };

  // use for cookie sign key, should change to your own and keep security

  config.keys = appInfo.name + "_1601166463755_9467";

  // add your middleware config here
  config.middleware = ["jwtVerity", "errorHandler"];

  config.security = {
    csrf: {
      enable: false,
    },

    domainWhiteList: ["*"],
  };
  config.cors = {
    origin: "*",
    allowMethods: "GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS",
  };

  config.mongoose = {
    client: {
      url: "mongodb://127.0.0.1:27017/ant-pro",

      options: {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        auth: { authSource: "admin" },
        user: "root",
        pass: "123456",
      },
      // mongoose global plugins, expected a function or an array of function and options
      // plugins: [createdPlugin, [updatedPlugin, pluginOptions]],
    },
  };

  (config.multipart = {
    fileExtensions: [".png", ".jpg", ".mp4", ".mp3", ".doc", ".ppt", ".pdf"],
    fileSize: "500mb",
  }),
    (config.jwtVerity = {
      secret: "123456",
      ignore: ["/login", "/getCode", "/swagger-doc", "/swagger-ui*", "/web*"],
    });

  config.swaggerdoc = {
    dirScanner: "./app/controller", // 配置自动扫描的控制器路径
    // 接口文档的标题，描述或其它
    apiInfo: {
      title: "egg-swagger", // 接口文档的标题
      description: "swagger-ui for egg api文档.", // 接口文档描述
      version: "1.0.0", // 接口文档版本
    },
    schemes: ["http", "https"], // 配置支持的协议
    consumes: ["application/json"], // 指定处理请求的提交内容类型（Content-Type），例如application/json,
    produces: ["application/json"], // 指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回
    securityDefinitions: {
      // 配置接口安全授权方式
      // apikey: {
      //   type: "apiKey",
      //   name: "Authorization",
      //   in: "header",
      // },
      // oauth2: {
      //   type: "oauth2",
      //   tokenUrl: "http://127.0.0.1:7002/login",
      //   flow: "password",
      //   scopes: {
      //     "write:access_token": "write access_token",
      //     "read:access_token": "read access_token",
      //   },
      // },
    },
    // enableSecurity: true,
  };
  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };

  return {
    ...config,
    ...userConfig,
  };
};
