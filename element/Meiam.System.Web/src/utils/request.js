import axios from 'axios'
import store from '../store'
import {Notification, MessageBox} from 'element-ui'
import {getToken, setToken} from '@/utils/auth'

const service = axios.create({
  // api 的 base_url
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 30000

})

// request拦截器
service.interceptors.request.use(
  config => {
    if (getToken()) {
      // 让每个请求携带自定义token 请根据实际情况自行修改
      config.headers['SYSTOKEN'] = getToken()
    }

    return config
  },
  error => {
    Promise.reject(error)
  }
)

// response 拦截器
service.interceptors.response.use(
  response => {
    // loadingInstance.close()
    // 延续 Token 时间
    const SysToken = getToken()
    if (SysToken) {
      setToken(SysToken)
    }

    const res = response.data
    if (res.statusCode !== 200) {
      switch (res.statusCode) {
        case 401:
          MessageBox.confirm(
            '登录状态已过期，您可以继续留在该页面，或者重新登录',
            '系统提示',
            {
              confirmButtonText: '重新登录',
              cancelButtonText: '取消',
              type: 'warning'
            }
          ).then(() => {
            store.dispatch('LogOut').then(() => {
              location.reload()
            })
          })
          break
        case 403:
          Notification.error({
            title: '您无权进行此操作，请求执行已拒绝',
            duration: 3000
          })
          break
        default:
          Notification.error({
            title: res.message
          })

          break
      }

      return Promise.reject(res.message)
    }

    return res
  },
  error => {
    Notification.error({
      title: '请求接口失败',
      duration: 3000
    })
    return Promise.reject(error)
  }
)
export default service
