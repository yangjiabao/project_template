const Controller = require("egg").Controller;
class RolesController extends Controller {
  // 获取角色
  async show() {
    const { ctx, service } = this;
    // await ctx.helper.authorize("roles_show");
    let req = ctx.query;
    let res = await service.roles.show(req);
    ctx.helper.success({ res });
  }

  // 创建角色
  async create() {
    const { ctx, service } = this;
    await ctx.helper.authorize("roles_create");
    let req = ctx.request.body;
    await service.roles.create(req);
    ctx.helper.success();
  }

  // 更新角色
  async update() {
    const { ctx, service } = this;
    await ctx.helper.authorize("roles_update");
    let req = ctx.request.body;
    await service.roles.update(req);
    ctx.helper.success();
  }

  // 删除角色
  async delete() {
    const { ctx, service } = this;
    await ctx.helper.authorize("roles_delete");
    ctx.validate({ _id: "array" });
    let req = ctx.request.body;
    await service.roles.delete(req);
    ctx.helper.success();
  }
}
module.exports = RolesController;
