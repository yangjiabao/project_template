import Vue from 'vue'

import { Notification, MessageBox, Message } from 'element-ui'

export default function () {
  Vue.prototype.deleteConfirm = function (callback, data) {
    return new Promise(function (resolve, reject) {
      if (data instanceof Array && data.length === 0) {
        Message.error({
          message: '请选中需要删除数据项！',
        })
      } else {
        MessageBox.confirm('此操作将删除选中数据项, 是否继续?', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning',
        })
          .then(() => {
            callback(data).then(() => {
              resolve()
              Notification.success({
                title: '成功',
                message: '已成功删除数据！',
              })
            })
          })
          .catch((err) => {
            console.log(err)
            // Message.info({
            //   message: '已取消删除数据！',
            // })
          })
      }
    })
  }
}
