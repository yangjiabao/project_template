import request from '@/utils/request'

export function queryGift(data) {
  return request({
    url: '/api/Gift/viewQuery',
    method: 'post',
    data,
  })
}

export function createGift(data) {
  return request({
    url: '/api/Gift/Add',
    method: 'post',
    data,
  })
}

export function updateGift(data) {
  return request({
    url: '/api/Gift/update',
    method: 'post',
    data,
  })
}

export function deleteGift(data) {
  return request({
    url: '/api/Gift/delete',
    method: 'delete',
    data,
  })
}

export function uploadGiftPic(data) {
  return request({
    url: '/api/Gift/PicUpload',
    method: 'post',
    data,
    loading: true,
    loadingText: '上传图片中...',
    notify: true,
  })
}
