
// Generate four random hex digits. 
function s4() { 
  return ((1 + Math.random()) * 0x10000 | 0).toString(16).substring(1) 
} 
// Generate a pseudo-GUID by concatenating random hexadecimal. 
export default function createGuid() { 
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4() 
}

