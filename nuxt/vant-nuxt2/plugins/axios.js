
import { Toast } from 'vant';



export default function ({ $axios,redirect }) {
    
  // 请求拦截处理
    $axios.onRequest((config) => {

console.log(config.headers.common,localStorage.getItem('token'))

      config.headers.common.SYSTOKEN = localStorage.getItem('token');


      
// 网络请求超过一秒是显示加载中提示
      config.loadingTimeout = setTimeout(() => {

    
        Toast.loading({
            message: '加载中...',
            forbidClick: true,
            duration:0
          });


 },1000)




    })

  // 响应拦截处理

    $axios.onResponse((response) => {

console.log(response,'response')



          // 清除加载中提示定时器

          clearTimeout(response.config.loadingTimeout)
          // 清除加载中提示
      
                  Toast.clear()




      if(response.data.statusCode === 200){





        if(response.config.toast){



          Toast.success(response.data.message);
        
        
        
        }


return Promise.resolve(response.data);


      }else{


        switch (response.data.statusCode) {

          case 401 :
            redirect('/login')
            // Toast.fail(response.data.message);
      
        break;
        
        default :
        
                // Toast.fail(response.data.message);
        break
        
        
        
        
        }
        
        
        
        return Promise.reject(response);



      }







      })



        // 错误拦截处理
    $axios.onError((error) => {


      console.log({...error})



  
      
            if(error.response === undefined){



              // 当走onResponse回调中reject时
              if(error.status === 200){




                Toast.fail(error.data.message);
            



              }else{



                   // 清除加载中提示定时器
                   clearTimeout(error.config.loadingTimeout)
          
                   // 清除加载中提示
               
                       Toast.clear()




      Toast.fail(error.message);


              }

           
      
      
              
          
      
      
            }else{

    





                    // 清除加载中提示定时器
                    clearTimeout(error.config.loadingTimeout)
          
                    // 清除加载中提示
                
                        Toast.clear()
           
          
          
                        Toast.fail(error.message);




              
            }
      
            return Promise.reject(error);
      
      
      
      
      
      
          })

    

  }
  