module.exports = {
  //网站标题
  title: '黄埔古迹系统',
  //是否显示 显示导航标签
  tagsView: true,
  //固定头部
  fixedHeader: true,

  //是否显示logo
  sidebarLogo: true,

  // 布局方式
  layoutMode: 1,

  // 显示头部标题
  headerTitleShow: false,

  logoShow: false,
}
