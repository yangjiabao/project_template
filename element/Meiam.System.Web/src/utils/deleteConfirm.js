
import Vue from 'vue'

import {Notification, MessageBox, Message} from 'element-ui'

Vue.prototype.deleteConfirm = function(callback, data) {
  return new Promise(function(resolve) {
    MessageBox.confirm('此操作将永久删除该数据项, 是否继续?', '提示', {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'warning'
    }).then(() => {
      callback(data).then(() => {
        resolve()

        Notification.success({
          title: '成功',
          message: '删除数据项成功！'
        })

  
      })
    }).catch(() => {
      Message.success({
        message: '已取消删除数据项！'
      })

             
    })
  })
}
 