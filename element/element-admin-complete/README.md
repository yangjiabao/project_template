# Meiam.System.Web

Meiam.System 前端源码

#### 前端模板

初始模板基于： [https://github.com/elunez/eladmin-web](https://github.com/elunez/eladmin-web)

模板文档： [https://docs.auauz.net/#/](https://docs.auauz.net/#/)

#### Build Setup
``` bash
# 配置镜像加速
npm config set registry https://registry.npm.taobao.org

# 安装依赖
npm install

# 启动服务 localhost:18888
npm run dev

# 构建生产环境
npm run build

```

#### VSCODE 配置

请安装 ESLint & Vetur & Element UI Snippets & Vue 2 Snippets 插件




#### 项目功能

1、ELEMENT UI 2.15.1
2、VUE 2.0
3、Eslint
4、权限管理模块
5、拖拽表格
6、公共样式
7、ELEMENT UI重置样式
8、







