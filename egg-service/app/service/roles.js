const Service = require("egg").Service;

class RolesService extends Service {
  async show(req) {
    const { ctx } = this;

    if (req.pageIndex && req.pageSize) {
      let data = await ctx.model.Roles.find()
        .skip((req.pageIndex - 1) * req.pageSize)
        .limit(Number(req.pageSize));
      let count = await ctx.model.Roles.countDocuments();

      return { data, count };
    } else {
      let data = await ctx.model.Roles.find();

      return data;
    }
  }

  async create(req) {
    const { ctx } = this;
    await ctx.model.Roles.insertMany(req);
  }

  async update(req) {
    const { ctx } = this;
    await ctx.model.Roles.updateOne({ _id: req._id }, req);
  }

  async delete(req) {
    const { ctx } = this;
    await ctx.model.Roles.deleteMany({ _id: { $in: req._id } });
    await ctx.model.RolesUser.deleteMany({ roles_id: { $in: req._id } });
    await ctx.model.RolesPermission.deleteMany({ roles_id: { $in: req._id } });
  }
}

module.exports = RolesService;
