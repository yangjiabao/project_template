/**
 * @param {string} url
 * @returns {Object}
 */
export function getQueryObject(url) {
  url = url == null ? window.location.href : url
  const search = url.substring(url.lastIndexOf('?') + 1)
  const obj = {}
  const reg = /([^?&=]+)=([^?&=]*)/g
  search.replace(reg, (rs, $1, $2) => {
    const name = decodeURIComponent($1)
    let val = decodeURIComponent($2)
    val = String(val)
    obj[name] = val
    return rs
  })
  return obj
}

/**
 * @param {string} input value
 * @returns {number} output value
 */
export function byteLength(str) {
  // returns the byte length of an utf8 string
  let s = str.length
  for (var i = str.length - 1; i >= 0; i--) {
    const code = str.charCodeAt(i)
    if (code > 0x7f && code <= 0x7ff) s++
    else if (code > 0x7ff && code <= 0xffff) s += 2
    if (code >= 0xdc00 && code <= 0xdfff) i--
  }
  return s
}

/**
 * @param {string} url
 * @returns {Object}
 */
export function param2Obj(url) {
  const search = url.split('?')[1]
  if (!search) {
    return {}
  }
  return JSON.parse(
    '{"' +
      decodeURIComponent(search)
        .replace(/"/g, '\\"')
        .replace(/&/g, '","')
        .replace(/=/g, '":"')
        .replace(/\+/g, ' ') +
      '"}'
  )
}

/**
 * @param {HTMLElement} element
 * @param {string} className
 */
export function toggleClass(element, className) {
  if (!element || !className) {
    return
  }
  let classString = element.className
  const nameIndex = classString.indexOf(className)
  if (nameIndex === -1) {
    classString += '' + className
  } else {
    classString =
      classString.substr(0, nameIndex) +
      classString.substr(nameIndex + className.length)
  }
  element.className = classString
}

/**
 * Check if an element has a class
 * @param {HTMLElement} elm
 * @param {string} cls
 * @returns {boolean}
 */
export function hasClass(ele, cls) {
  return !!ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'))
}

/**
 * Add class to element
 * @param {HTMLElement} elm
 * @param {string} cls
 */
export function addClass(ele, cls) {
  if (!hasClass(ele, cls)) ele.className += ' ' + cls
}

/**
 * Remove class from element
 * @param {HTMLElement} elm
 * @param {string} cls
 */
export function removeClass(ele, cls) {
  if (hasClass(ele, cls)) {
    const reg = new RegExp('(\\s|^)' + cls + '(\\s|$)')
    ele.className = ele.className.replace(reg, ' ')
  }
}

// 替换邮箱字符
export function regEmail(email) {
  if (String(email).indexOf('@') > 0) {
    const str = email.split('@')
    let _s = ''
    if (str[0].length > 3) {
      for (var i = 0; i < str[0].length - 3; i++) {
        _s += '*'
      }
    }
    var new_email = str[0].substr(0, 3) + _s + '@' + str[1]
  }
  return new_email
}

// 替换手机字符
export function regMobile(mobile) {
  if (mobile.length > 7) {
    var new_mobile = mobile.substr(0, 3) + '****' + mobile.substr(7)
  }
  return new_mobile
}

// 下载文件
export function downloadFile(file) {
  let a = ''
  a = document.createElement('a')
  a.style.display = 'none'
  let timeStamp = new Date().getTime()

  console.log(typeof file === 'string')
  // 判断传入为链接还是文件流
  if (typeof file === 'string') {
    let baseUrl = process.env.VUE_APP_BASE_API // 若是不完整的url则需要拼接baseURL
    let completeUrl = file.includes('http') ? file : baseUrl + file // 完整的url则直接使用

    // 这里是将url转成blob地址，
    fetch(completeUrl)
      .then((res) => res.blob())
      .then((blob) => {
        console.log(URL.createObjectURL(blob))

        // 将链接地址字符内容转变成blob地址
        a.href = URL.createObjectURL(blob)
        // 下载文件的名字
        a.download = `${timeStamp}_${
          completeUrl.split('/')[completeUrl.split('/').length - 1]
        }`

        document.body.appendChild(a)
        a.click()
        document.body.removeChild(a)
      })
  } else {
    a.href = URL.createObjectURL(new Blob([file]))
    a.download = `${timeStamp}_${file.name}`

    document.body.appendChild(a)
    a.click()
    document.body.removeChild(a)
  }
}

/**
 * 构造树型结构数据
 * @param {*} data 数据源
 * @param {*} id id字段 默认 'id'
 * @param {*} parentId 父节点字段 默认 'parentId'
 * @param {*} children 孩子节点字段 默认 'children'
 * @param {*} rootId 根Id 默认 0
 */
export function handleTree(data, id, parentId, children, rootId) {
  id = id || 'id'
  parentId = parentId || 'parentId'
  children = children || 'children'
  rootId = rootId || 0
  // 对源数据深度克隆
  const cloneData = JSON.parse(JSON.stringify(data))
  // 循环所有项
  const treeData = cloneData.filter((father) => {
    const branchArr = cloneData.filter((child) => {
      // 返回每一项的子级数组
      return father[id] === child[parentId]
    })
    branchArr.length > 0 ? (father.children = branchArr) : ''
    // 返回第一层
    return father[parentId] === rootId
  })
  return treeData !== '' ? treeData : data
}

// 计算两个年月日日期相隔天数
export function computeDistanceDay(startDateString, endDateString) {
  let separator = '-' //日期分隔符
  let startDates = startDateString.split(separator)
  let endDates = endDateString.split(separator)
  let startDate = new Date(startDates[0], startDates[1] - 1, startDates[2])
  let endDate = new Date(endDates[0], endDates[1] - 1, endDates[2])
  return parseInt(Math.abs(endDate - startDate) / 1000 / 60 / 60 / 24) //把相差的毫秒数转换为天数
}

// Generate a pseudo-GUID by concatenating random hexadecimal.
export function guid() {
  // 生成guid（唯一标识码）
  function s4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
  }

  return (
    s4() +
    s4() +
    '-' +
    s4() +
    '-' +
    s4() +
    '-' +
    s4() +
    '-' +
    s4() +
    s4() +
    s4()
  )
}

// 创建script标签引入外部js
export function createScript(src) {
  return new Promise((resolve, reject) => {
    let script = document.querySelector(`script[src="${src}"]`)

    if (!script) {
      let script = document.createElement('script')

      script.type = 'text/javascript'

      script.src = src

      script.addEventListener('load', () => resolve(script))

      script.addEventListener('error', (event) => reject(event))

      script = document.head.appendChild(script)
    } else {
      reject('exist')
    }
  })
}

//百度地图转腾讯地图经纬度

export function BdMapToTxMap(lng, lat) {
  let x_pi = (Math.PI * 3000.0) / 180.0
  let x = lng - 0.0065
  let y = lat - 0.006
  let z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * x_pi)
  let theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * x_pi)
  let lngs = z * Math.cos(theta)
  let lats = z * Math.sin(theta)
  return {
    lng: lngs,
    lat: lats,
  }
}

//腾讯地图转百度地图经纬度

export function TxMapToBdMap(lng, lat) {
  let x_pi = (Math.PI * 3000.0) / 180.0 //Math.PI ~ 3.14159265358979324
  let x = lng
  let y = lat
  let z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * x_pi)
  let theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * x_pi)
  let lngs = z * Math.cos(theta) + 0.0065
  let lats = z * Math.sin(theta) + 0.006
  return {
    lng: lngs,
    lat: lats,
  }
}

// 表格行合并
export function tableRowspan({
  row,
  column,
  rowIndex,
  columnIndex,
  // 表格数据
  data,
  // 需要合并列,默认合并第一列
  index = [0],
}) {
  // 需要合并行
  if (index.includes(columnIndex)) {
    let preRow = data[rowIndex - 1]

    let nextRow = data[rowIndex + 1]

    // 需要合并属性
    let property = column.property

    // 需要合并属性的值

    let cellValue = row[property]

    // 当前行已被合并则设置rowspan为0
    if (preRow && preRow[property] === cellValue) {
      return { rowspan: 0, colspan: 0 }
    } else {
      // 当前行未被合并则向下相加rowspan

      let rowspan = 1
      while (nextRow && nextRow[property] === cellValue) {
        rowspan++

        nextRow = data[rowspan + rowIndex]
      }
      if (rowspan > 1) {
        return { rowspan, colspan: 1 }
      }
    }
  }
}
