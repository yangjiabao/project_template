import Vue from 'vue'
import Router from 'vue-router'
import Layout from '../layout/index'

Vue.use(Router)

export const constantRouterMap = [
  {
    path: '/login',
    name: 'login',
    meta: { title: '登录', keepAlive: true },
    component: (resolve) => require(['@/views/login'], resolve),
    hidden: true,
  },
  {
    path: '/404',
    component: (resolve) => require(['@/views/features/404'], resolve),
    hidden: true,
  },
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: (resolve) => require(['@/views/features/redirect'], resolve),
      },
    ],
  },
  {
    path: '/',
    component: Layout,
    redirect: '/home',
    children: [
      {
        path: 'home',
        component: (resolve) => require(['@/views/home'], resolve),
        name: 'home',
        meta: {
          title: '系统首页',
          icon: 'home-fill',
          affix: true,
          keepAlive: true,
        },
      },
    ],
  },
  {
    path: '/users',
    component: Layout,
    redirect: '/users/center',
    children: [
      {
        path: 'center',
        component: (resolve) => require(['@/views/users/center'], resolve),
        name: 'home',
        hidden: true,
        meta: { title: '个人中心', icon: 'user-fill', keepAlive: true },
      },
    ],
  },
]

export default new Router({
  mode: 'hash',
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap,
})
