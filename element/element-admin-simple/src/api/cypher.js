import request from '@/utils/request'

export function queryCypher(data) {
  return request({
    url: '/api/Exchange/viewQuery',
    method: 'post',
    data,
  })
}
export function createCypher(data) {
  return request({
    url: '/api/Exchange/Add',
    method: 'post',
    data,
  })
}

export function deleteCypher(data) {
    return request({
      url: '/api/Exchange/delete',
      method: 'delete',
      data,
    })
  }
  

