// All configuration item explanations can be find in https://cli.vuejs.org/config/

let config = require('./src/config.js')

module.exports = {
  assetsDir: 'h5/static',

  configureWebpack: {
    name: config.title, // 网址标题
  },

  css: {
    loaderOptions: {
      css: {
        // options here will be passed to css-loader
      },
      postcss: {
        plugins: [
          require('postcss-pxtorem')({
            rootValue: 16,
            propList: ['*'],
            // 在配置 postcss-loader 时，应避免 ignore node_modules 目录，否则将导致 Vant 样式无法被编译
            // exclude: /node_modules/i,
          }),
        ],
      },
    },
  },
}
