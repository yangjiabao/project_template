import request from '@/utils/request'

export function getStreet() {
  return request({
    url: '/api/Guji/GetStreet',
    method: 'get',
  })
}

export function getCategory() {
  return request({
    url: '/api/Guji/GetCategory',
    method: 'get',
  })
}

export function getGrade(category) {
  return request({
    url: '/api/Guji/GetGrade?category=' + category,
    method: 'get',
  })
}

export function querySite(data) {
  return request({
    url: '/api/Guji/GetInforBySearch',
    method: 'post',
    data,
  })
}
