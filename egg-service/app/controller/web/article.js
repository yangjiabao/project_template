const Controller = require("egg").Controller;

class ArticleController extends Controller {
  async show() {

    const { ctx, service } = this;
    let req = ctx.query;
    let res = await service.web.article.show(req);

    ctx.helper.success({ res });
  }
  async detail() {
    const { ctx, service } = this;
    let req = ctx.params;


    let res = await service.web.article.detail(req);

    ctx.helper.success({ res });
  }

  async create() {
    const { ctx, service } = this;
    let req = ctx.request.body;
    await service.web.article.create(req);
    ctx.helper.success();
  }

  async update() {
    const { ctx, service } = this;
    let req = ctx.request.body;
    await service.web.article.update(req);
    ctx.helper.success();
  }

  async delete() {
    const { ctx, service } = this;
    let req = ctx.request.body;
    await service.web.article.delete(req);
    ctx.helper.success();
  }


}
module.exports = ArticleController;
