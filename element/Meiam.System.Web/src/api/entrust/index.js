import request from '@/utils/request'

// 咨询服务
export function addMaterial(data) {
  return request({
    url: '/api/ConsultationServer/AddMaterial',
    method: 'post',
    data
  })
}
    
export function getTempleMaterial(data) {
  return request({
    url: '/api/ConsultationServer/GetTempleMaterialDeforeAdd',
    method: 'post',
    data
  })
}

export function getConsultationList(data) {
  return request({
    url: '/api/ConsultationServer/GetConsultationListByRequireCode',
    method: 'post',
    data
  })
}

export function getMaterialModel(data) {
  return request({
    url: '/api/ConsultationServer/GetMaterialModel',
    method: 'post',
    data
  })
}

export function downMaterialModel(data) {
  return request({
    url: '/api/ConsultationServer/DownnMaterialModel',
    method: 'post',
    data
  })
}

export function wordToPDF(data) {
  return request({
    url: '/api/ConsultationServer/TranslateWordToPDF',
    method: 'post',
    data
  })
}

export function queryNetworkRequire() {
  return request({
    url: '/api/Assist/QueryRequireList',
    method: 'get'
    
  })
}

