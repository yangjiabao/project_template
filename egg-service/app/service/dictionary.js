const await = require("await-stream-ready/lib/await");

const Service = require("egg").Service;

class PermissionService extends Service {
  // 获取字典
  async show(req) {
    const { ctx } = this;
    // 通过get获取参数为字符串使用分页时需转为数字类型
    if (req.pageIndex && req.pageSize) {
      let data = await ctx.model.Dictionary.find()
        .skip((req.pageIndex - 1) * req.pageSize)
        .limit(Number(req.pageSize));

      // 获取集合中文档数
      let count = await ctx.model.Dictionary.countDocuments();
      return { data, count };
    } else {
      let data = await ctx.model.Dictionary.find();
      // let count = await ctx.model.Dictionary.countDocuments();
      return { data };
    }
  }

  // 创建字典
  async create(req) {
    const { ctx } = this;
    await ctx.model.Dictionary.insertMany(req);
  }

  async update(req) {
    const { ctx } = this;
    // findByIdAndUpdate更新后返回更新文档
    await ctx.model.Dictionary.updateOne({ _id: req._id }, req);
  }

  // 删除字典
  async delete(req) {
    const { ctx } = this;
    // $in传数组进行批量操作
    await ctx.model.Dictionary.deleteMany({ _id: { $in: req._id } });

    await ctx.model.Permission.deleteMany({ dictionary_id: { $in: req._id } });
  }
}

module.exports = PermissionService;
