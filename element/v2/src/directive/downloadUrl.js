// 点击链接下载

export default {
  bind(el, binding) {
    // 修改鼠标移入样式为手指
    el.style.cursor = 'pointer'
    //监听点击事件
    el.addEventListener('click', () => {
      console.log(1212121)
      const a = document.createElement('a')
      let baseUrl = process.env.VUE_APP_BASE_API // 若是不完整的url则需要拼接baseURL
      const url = binding.value.includes('http')
        ? binding.value
        : baseUrl + binding.value // 完整的url则直接使用
      // 这里是将url转成blob地址，
      fetch(url)
        .then((res) => res.blob())
        .then((blob) => {
          // 将链接地址字符内容转变成blob地址
          a.href = URL.createObjectURL(blob)
          a.download = url.split('/')[url.split('/').length - 1] //  // 下载文件的名字
          document.body.appendChild(a)
          a.click()
        })
    })
  },
}
