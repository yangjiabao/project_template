import request from '@/utils/request'

export function queryAccount(data) {
  return request({
    url: '/api/Users/VipQuery',
    method: 'post',
    data,
  })
}
export function createAccount(data) {
  return request({
    url: '/api/Users/Reg',
    method: 'post',
    data,
  })
}
