const Service = require("egg").Service;

class MenuService extends Service {
  async show() {
    const { ctx } = this;

    let childMenu = await ctx.model.WebMenu.find({
      parent_id: { $ne: "-1" },
    })
      .sort("sort")
      .lean();

    let parentMenu = await ctx.model.WebMenu.find({ parent_id: "-1" })
      .sort("sort")
      .lean();

    iterateChild(parentMenu, childMenu);

    function iterateChild(parentMenu, childMenu) {
      for (let parentItem of parentMenu) {
        for (let childItem of childMenu) {
          if (parentItem._id.equals(childItem.parent_id)) {
            parentItem.children = parentItem.children
              ? [...parentItem.children, childItem]
              : [childItem];
          }
        }

        if (parentItem.children) {
          iterateChild(parentItem.children, childMenu);
        }
      }
    }

    return parentMenu;
  }

  async create(req) {
    const { ctx } = this;

    await ctx.model.WebMenu.insertMany(req);
  }

  async delete(req) {
    const { ctx } = this;

    // let menu_id = await ctx.model.WebMenu.deleteMany({
    //   $or: [{ _id: req._id }, { parent_id: req._id }],
    // }).distinct("_id");

    let menu_id = await ctx.model.WebMenu.deleteMany({
      _id: { $in: req._id },
    }).lean();

    console.log(menu_id);

    // iterateDelete(menu_id);

    // async function iterateDelete(menu_id) {
    //   if (menu_id.length > 0) {
    //     for (let item of menu_id) {
    //       let childMenu_id = await ctx.model.WebMenu.deleteMany({
    //         $or: [{ _id: item }, { parent_id: item }],
    //       }).distinct("_id");

    //       if (childMenu_id.length > 0) {
    //         iterateDelete(childMenu_id);
    //       }
    //     }
    //   }
    // }
  }

  async update(req) {
    const { ctx } = this;
    await ctx.model.WebMenu.findByIdAndUpdate(req._id, req);
  }
}

module.exports = MenuService;
