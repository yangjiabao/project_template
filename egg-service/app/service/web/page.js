const Service = require("egg").Service;

class PageService extends Service {
  // 获取字典
  async show(req) {
    const { ctx } = this;

    //     let data = await ctx.model.Dictionary.find()
    //     .skip((req.pageIndex - 1) * req.pageSize)
    //     .limit(Number(req.pageSize));

    //   // 获取集合中文档数
    //   let count = await ctx.model.Dictionary.countDocuments();
    //   return { data, count };

    let data = await ctx.model.Page.find()
      .skip((req.pageIndex - 1) * req.pageSize)
      .limit(Number(req.pageSize));

    let count = await ctx.model.Page.countDocuments();
    return { data, count };
  }

  async create(req) {
    const { ctx } = this;

    await ctx.model.Page.insertMany(req);
  }

  async delete(req) {
    const { ctx } = this;

    await ctx.model.Page.findByIdAndDelete(req._id);
  }

  async update(req) {
    const { ctx } = this;
    await ctx.model.Page.findByIdAndUpdate(req._id, req);
  }
}

module.exports = PageService;
