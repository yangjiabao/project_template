import request from '@/utils/request'

export function queryCategory(data) {
  return request({
    url: '/api/Column/viewQuery',
    method: 'post',
    data,
    toast: true,
  })
}

export function getUserInfo(params) {
  return request({
    url: '/api/Auth/GetUserInfo',
    method: 'get',
    params,
  })
}

export function getStreet() {
  return request({
    url: '/api/Guji/GetStreet',
    method: 'get',
  })
}

export function getCategory() {
  return request({
    url: '/api/Guji/GetCategory',
    method: 'get',
  })
}

export function querySite(data) {
  return request({
    url: '/api/Guji/GetInforBySearch',
    method: 'post',
    data,
  })
}

export function getSiteDetail(data) {
  return request({
    url: '/api/Guji/GetDetailByID',
    method: 'post',
    data,
  })
}

export function getGrade(category) {
  return request({
    url: '/api/Guji/GetGrade?category=' + category,
    method: 'get',
  })
}

export function queryChart(data) {
  return request({
    url: '/api/Guji/GetInforByStatistics',
    method: 'post',
    data,
  })
}
