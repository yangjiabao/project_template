const Service = require("egg").Service;

class PermissionService extends Service {
  // 获取权限
  async show(req) {
    const { ctx } = this;
    let data = await ctx.model.Permission.find()
      .skip((req.pageIndex - 1) * req.pageSize)
      .limit(Number(req.pageSize))
      .lean();
    let count = await ctx.model.Permission.countDocuments();
    for (let item of data) {
      // 第二个参数(projection)定义返回字段,对象类型({title:1/-1}) 1为返回,-1为不返回,1和-1不可同时出现(_id)除外。简写可以直接字段名，-describe则为不返回
      let menuDocs = await ctx.model.Menu.findById(item.menu_id, "title");
      let dictionaryDocs = await ctx.model.Dictionary.findById(
        item.dictionary_id,
        "describe"
      );
      item.menuTitle = menuDocs.title;
      item.dictionaryDescribe = dictionaryDocs.describe;
    }
    return { data, count };
  }

  // 创建权限
  async create(req) {
    const { ctx } = this;
    await ctx.model.Permission.insertMany(req);
  }

  // 更新权限
  async update(req) {
    const { ctx } = this;
    await ctx.model.Permission.updateOne({ _id: req._id }, req);
  }

  // 删除权限
  async delete(req) {
    const { ctx } = this;
    await ctx.model.Permission.deleteMany({ _id: { $in: req._id } });
  }

  // 获取菜单权限
  async getMenuPermission() {
    const { ctx } = this;

    // $ne和$nin表示该值除外。但nin接数组
    let parent_id = await ctx.model.Menu.find({
      parent_id: { $ne: "-1" },
    }).distinct("parent_id");

    // lean()表示将mongdb文档类型转为对象类型。mongdb文档时不可操作scheme没定义字段，转为普通对象即可操作
    let data = await ctx.model.Menu.find({ _id: { $nin: parent_id } }).lean();

    for (let item of data) {
      let permissionDocs = await ctx.model.Permission.find({
        menu_id: item._id,
      }).lean();

      for (let permissionItem of permissionDocs) {
        let dictionaryDocs = await ctx.model.Dictionary.findById(
          permissionItem.dictionary_id
        );

        permissionItem.dictionaryDescribe = dictionaryDocs.describe;
      }
      item.permission = permissionDocs;
    }

    return data;
  }

  // 获取角色权限
  async getRolesPermission(req) {
    const { ctx } = this;
    let data = await ctx.model.RolesPermission.find(req).distinct(
      "permission_id"
    );

    return data;
  }

  // 更新角色权限
  async updateRolesPermission(req) {
    const { ctx } = this;
    // 先删除该角色下所有权限，再重现添加
    await ctx.model.RolesPermission.deleteMany({ roles_id: req[0].roles_id });

    if (req[0].menu_id) {
      await ctx.model.RolesPermission.insertMany(req);
    }
  }
}

module.exports = PermissionService;
