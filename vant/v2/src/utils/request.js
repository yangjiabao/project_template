import axios from 'axios'

import router from '@/router'

import { Toast, Dialog } from 'vant'

let loadingToast = ''

const service = axios.create({
  // api 的 base_url
  baseURL: process.env.VUE_APP_BASE_API,
  // 超时
  timeout: 30000,
})

// request拦截器
service.interceptors.request.use(
  (config) => {
    config.headers.SYSTOKEN = localStorage.getItem('token')

    // 全局AXIOS请求加载Message。只适用于单个接口处理
    // 网络请求超过一秒是显示加载中提示
    config.loadingTimeout = setTimeout(() => {
      loadingToast = Toast.loading({
        message: '加载中...',
        forbidClick: true,
        duration: 0,
      })
    }, 1000)

    return config
  },
  (error) => {
    // 清除加载中提示定时器
    clearTimeout(error.config.loadingTimeout)

    // 清除加载中提示
    loadingToast && loadingToast.clear()

    Promise.reject(error)
  }
)

// response 拦截器
service.interceptors.response.use(
  (response) => {
    // 清除加载中提示定时器
    clearTimeout(response.config.loadingTimeout)

    // 清除加载中提示
    loadingToast && loadingToast.clear()

    // 返回数据
    let res = response.data

    // 返回状态码不等于200
    if (res.statusCode !== 200) {
      switch (res.statusCode) {
        // 401登录过期
        case 401:
          Dialog.confirm({
            title: '系统提示',
            message: '登录状态已过期，请重新登录',
          })
            .then(() => {
              router.push('/login')
            })
            .catch(() => {})
          break
        default:
          Toast.fail(res.message)
          break
      }

      return Promise.reject(res.message)
    }

    // 请求接口成功提示
    if (response.config.toast) {
      Toast.success(res.message)
    }

    return res
  },
  (error) => {
    clearTimeout(error.config.loadingTimeout)

    Toast.fail(error.message)

    return Promise.reject(error)
  }
)
export default service
