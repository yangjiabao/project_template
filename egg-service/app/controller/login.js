const Controller = require("egg").Controller;

class LoginController extends Controller {
  // 登录接口
  async login() {
    const { ctx, service } = this;
    let req = ctx.request.body;
    await service.login.login(req);
  }

  // 登录接口
  async logout() {
    const { ctx, service } = this;
    await service.login.logout();
    ctx.helper.success({ ctx });
  }

  async getCode() {
    const { ctx, service } = this;

    let res = await service.login.getCode();

    ctx.helper.success({ ctx, res });
  }
}
module.exports = LoginController;
