import Vue from 'vue'

import Cookies from 'js-cookie'
import Element from 'element-ui'

// global css
import 'normalize.css/normalize.css'
import './assets/styles/element-variables.scss'
import './assets/styles/index.scss'
// import 'element-ui/lib/theme-chalk/base.css'

import './assets/icons' // icon
import './router/index' //

import App from './App'
import store from './store'
import router from './router/routers'
import Pagination from './components/Pagination'

import directive from './directive'

import ContextMenu from './components/ContextMenu'

import deleteConfirm from './utils/deleteConfirm'

// lodash工具函数库
import _ from 'lodash'

// 树下拉
import Treeselect from '@riophae/vue-treeselect'
import '@riophae/vue-treeselect/dist/vue-treeselect.css'

// 全局方法挂载
Vue.prototype._ = _

// 全局组件挂载
Vue.component('Pagination', Pagination)
Vue.component('ContextMenu', ContextMenu)

Vue.component('TreeSelect', Treeselect)

Vue.use(deleteConfirm)

// 全局注册指令
Vue.use(directive)

Vue.use(Element, {
  size: Cookies.get('size') || 'small', // set element-ui default size
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: (h) => h(App),
})
