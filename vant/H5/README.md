# hello-world

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



### 工具库
1、Lodash 是一个一致性、模块化、高性能的 JavaScript 实用工具库
2、ECharts图表库
3、vant组件库
4、dayjs日期处理库
5、postcss-pxtorem