module.exports = (app) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  let ObjectId = mongoose.Schema.Types.ObjectId;
  const RolesUserSchema = new Schema({
    roles_id: {
      required: true,
      type: ObjectId,
      ref: "roles",
    },
    user_id: {
      required: true,
      type: ObjectId,
      ref: "user",
    },
  });
  return mongoose.model("RolesUser", RolesUserSchema, "roles_user");
};
