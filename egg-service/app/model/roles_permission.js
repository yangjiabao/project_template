module.exports = (app) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  let ObjectId = Schema.Types.ObjectId;
  const RolesPermissionSchema = new Schema({

    menu_id: {
      required: true,
      type: ObjectId,
    },
    permission_id: {
      type: ObjectId,
    },
    roles_id: {
      required: true,
      type: ObjectId,
    },
  });
  return mongoose.model("RolesPermission", RolesPermissionSchema, "roles_permission");
};
