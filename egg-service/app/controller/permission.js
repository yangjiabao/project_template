const Controller = require("egg").Controller;

class PermissionController extends Controller {
  // 获取权限
  async show() {
    const { ctx, service } = this;
    // await ctx.helper.authorize("permission_show");
    let req = ctx.query;
    let res = await service.permission.show(req);
    ctx.helper.success({ res });
  }

  // 创建权限
  async create() {
    const { ctx, service } = this;

    await ctx.helper.authorize("permission_create");
    let req = ctx.request.body;
    await service.permission.create(req);
    ctx.helper.success();
  }

  // 更新权限
  async update() {
    const { ctx, service } = this;
    await ctx.helper.authorize("permission_update");
    let req = ctx.request.body;
    await service.permission.update(req);
    ctx.helper.success();
  }

  // 删除权限
  async delete() {
    const { ctx, service } = this;
    await ctx.helper.authorize("permission_delete");
    ctx.validate({ _id: "array" });
    let req = ctx.request.body;
    await service.permission.delete(req);
    ctx.helper.success();
  }
}
module.exports = PermissionController;
