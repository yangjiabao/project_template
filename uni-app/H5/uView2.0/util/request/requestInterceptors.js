/**
 * 请求拦截
 * @param {Object} http
 */
module.exports = (vm) => {
	uni.$u.http.interceptors.request.use((config) => { // 可使用async await 做异步操作
			console.log(config, 'config')
			config.header['SYSTOKEN'] = uni.getStorageSync('token')

			// 初始化请求拦截器时，会执行此方法，此时data为undefined，赋予默认{}
			config.data = config.data || {}
			// 可以在此通过vm引用vuex中的变量，具体值在vm.$store.state中
			// console.log(vm.$store.state);


			// 全局AXIOS请求加载Message。只适用于单个接口处理
			// 网络请求超过一秒是显示加载中提示



			config.loadingTimeout = setTimeout(() => {
				config.loadingToast = uni.showLoading({



					title: '加载中'
				})
			}, 1000)



			return config
		}, (config) => {



			clearTimeout(config.loadingTimeout)

			config.loadingToast && uni.hideLoading()

			Promise.reject(config)
		} // 可使用async await 做异步操作


	)
}
