const Service = require("egg").Service;
const svgCaptcha = require("svg-captcha");

class LoginService extends Service {
  async login(req) {
    const { ctx, app } = this;
    let username = req.username;
    let userDocs = await ctx.model.User.findOne({ username });
    if (userDocs) {
      let password = req.password;
      let userDocs = await ctx.model.User.findOne({ password, username });
      if (userDocs) {
        if (userDocs.status === 0) {
          ctx.helper.fail("账号被禁用");
        } else {
          let roles = await ctx.model.RolesUser.find({
            user_id: userDocs._id,
          }).distinct("roles_id");

          // 设置jwt中token
          let token = app.jwt.sign(
            // 存入数据
            {
              username,
              roles,
            },
            // 时效
            { expiresIn: "2h" },
            // 秘钥
            ctx.app.config.jwtVerity.secret
          );

          ctx.helper.success({ res: token, msg: "登录成功" });
        }
      } else {
        ctx.helper.fail("账号密码错误");
      }
    } else {
      ctx.helper.fail("账号不存在");
    }
  }

  async getCode() {
    let captcha = svgCaptcha.create({
      size: 4,
      fontSize: 60,
      width: 112,
      height: 46,
      background: "#ffffff",
    });

    return captcha;
  }

  async logout() {
    const { ctx } = this;
    let username = ctx.decode.username;
    await ctx.model.LoginUser.deleteOne({ "user.username": username });
  }
}

module.exports = LoginService;
