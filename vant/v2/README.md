# hello-world

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



### 工具库
1、Lodash 是一个一致性、模块化、高性能的 JavaScript 实用工具库  https://www.lodashjs.com/
2、ECharts图表库  https://echarts.apache.org/zh/index.html
3、vant组件库  https://vant-contrib.gitee.io/vant/v2/#/zh-CN/home
4、dayjs日期处理库  https://www.npmjs.com/package/dayjs
5、postcss-pxtorem  https://www.npmjs.com/package/postcss-pxtorem
6、md5加密  https://www.npmjs.com/package/md5