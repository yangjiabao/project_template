import Vue from 'vue'
import Vuex from 'vuex'

import { login, getUserInfo, loginOut } from '@/api/login'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: localStorage.getItem('token'),
    userInfo: {},
  },

  mutations: {
    setToken: (state, token) => {
      state.token = token
    },

    setUserInfo: (state, payload) => {
      state.userInfo = payload
    },
  },
  actions: {
    // 登录
    login({ commit }, payload) {
      return new Promise((resolve, reject) => {
        console.log(payload)
        login(payload)
          .then((res) => {
            // token存在localstorage中
            localStorage.setItem('token', res.data)

            commit('setToken', res.data)
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },

    // 退出登录
    loginOut({ commit }) {
      return new Promise((resolve, reject) => {
        loginOut()
          .then((res) => {
            localStorage.removeItem('token')

            commit('setToken', '')
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },

    // 获取用户信息
    getUserInfo({ commit }) {
      return new Promise((resolve, reject) => {
        getUserInfo()
          .then((res) => {
            commit('setUserInfo', res.data)
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
  },
  modules: {},
})
