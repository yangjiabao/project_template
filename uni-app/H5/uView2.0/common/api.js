
const {http} = uni.$u

// 获取菜单
export const fetchMenu = (params, config = {}) => http.post('/ebapi/public_api/index', params, config)

export const queryGift = (params, config = {custom: {successToast: true}}) => http.post('/api/Gift/viewQuery', params, config)

export const queryCategory = (params, config = {}) => http.post('/api/Column/viewQuery', params, config)

export const getUserInfo = (params, config = {}) => http.get('/api/Auth/GetUserInfo', params, config)
export const login = (params, config = {}) => http.post('/api/Auth/VipLogin', params, config)

