import request from '@/utils/request'

export function uploadPic(data) {
  return request({
    url: '/api/news/PicUpload',
    method: 'post',
    data,
  })
}
