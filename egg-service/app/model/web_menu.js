module.exports = (app) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const WebMenuSchema = new Schema({
    title: {
      required: true,
      type: String,
    },
    link: {
      type: String,
    },
    parent_id: {
      default: "-1",
      type: String,
    },

    sort: {
      type: Number,
    },
  });

  return mongoose.model("WebMenu", WebMenuSchema, "web_menu");
};
