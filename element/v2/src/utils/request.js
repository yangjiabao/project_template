import axios from 'axios'
import store from '../store'
import { Notification, MessageBox, Message } from 'element-ui'
import { getToken, setToken } from '@/utils/token'

let loadingMessage = null

const service = axios.create({
  // api 的 base_url
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 30000,
})

// request拦截器
service.interceptors.request.use(
  (config) => {
    if (getToken()) {
      // 让每个请求携带自定义token 请根据实际情况自行修改
      config.headers['SYSTOKEN'] = getToken()
    }

    // 全局AXIOS请求加载Message。只适用于单个接口处理
    if (config.loading) {
      loadingMessage = Message({
        iconClass: 'el-icon-loading',
        dangerouslyUseHTMLString: true,
        message: `<span style="margin-left:15px;color:#606266">${
          config.loadingText ? config.loadingText : '请求数据接口中...'
        }</span>`,
        duration: 0,
      })
    }

    return config
  },
  (error) => {
    Promise.reject(error)
  }
)

// response 拦截器
service.interceptors.response.use(
  (response) => {
    // 手动关闭所有Message实例
    loadingMessage && loadingMessage.close()
    // 延续 Token 时间
    const SysToken = getToken()
    if (SysToken) {
      setToken(SysToken)
    }

    const res = response.data
    if (res.statusCode !== 200) {
      switch (res.statusCode) {
        case 401:
          MessageBox.confirm(
            '登录状态已过期，您可以继续留在该页面，或者重新登录',
            '系统提示',
            {
              confirmButtonText: '重新登录',
              cancelButtonText: '取消',
              type: 'warning',
            }
          ).then(() => {
            store.dispatch('LogOut').then(() => {
              location.reload()
            })
          })
          break
        case 403:
          Notification.error({
            title: '错误',
            message: '您无权进行此操作，请求执行已拒绝',
          })
          break
        default:
          Notification.error({
            title: '错误',
            message: res.message,
          })

          break
      }

      return Promise.reject(res.message)
    }

    console.log(response)

    // 请求接口成功提示
    if (response.config.notify) {
      Notification.success({
        title: '成功',
        message: res.message,
      })
    }

    return res
  },
  (error) => {
    Notification.error({
      title: '错误',
      message: `${error.response.statusText}（${error.response.status}）`,
    })
    return Promise.reject(error)
  }
)
export default service
