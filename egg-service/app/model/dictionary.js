module.exports = (app) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const DictionarySchema = new Schema({
    name: {
      // 唯一键值
      unique: true,
      // 必须键值
      required: true,
      type: String,
    },

    describe: {
      required: true,
      type: String,
    },

    // 自动生成创建日期
    createTime: {
      type: Date,
      default: Date.now,
    },
  });

  // 第三个参数值为指定文档名。不知定需注意复数
  return mongoose.model("Dictionary", DictionarySchema, "dictionary");
};
