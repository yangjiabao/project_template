module.exports = (app) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  const ObjectId = Schema.Types.ObjectId;

  const PermissionSchema = new Schema({
    menu_id: {
      required: true,
      type: ObjectId,
      ref: "menu",
    },
    dictionary_id: {
      required: true,
      type: ObjectId,
      ref: "dictionary",
    },
    name: {
      unique: true,
      required: true,
      type: String,
    },
    describe: {
      type: String,
    },

    createTime: {
      type: Date,
      default: Date.now,
    },
  });
  return mongoose.model("Permission", PermissionSchema, "permission");
};
