const Controller = require("egg").Controller;

class MenuController extends Controller {
  async show() {
    const { ctx, service } = this;
    let req = ctx.query;
    let res = await service.web.menu.show(req);

    ctx.helper.success({ res });
  }

  async create() {
    const { ctx, service } = this;
    let req = ctx.request.body;
    await service.web.menu.create(req);

    ctx.helper.success();
  }

  async delete() {
    const { ctx, service } = this;
    let req = ctx.request.body;
    await service.web.menu.delete(req);

    ctx.helper.success();
  }

  async update() {
    const { ctx, service } = this;
    let req = ctx.request.body;
    await service.web.menu.update(req);

    ctx.helper.success();
  }
}
module.exports = MenuController;
