module.exports = {
  root: true,
  // 解析器选项。指定你想支持的语言，默认支持es5
  parserOptions: {
    ecmaVersion: 6,
    parser: 'babel-eslint',
    sourceType: 'module'
  },


  env: {
    node: true // 只需将该项设置为 true 即可
  },

  globals: {

    "BMap": true,
  },



  extends: ["plugin:vue/essential", "eslint:recommended", "@vue/prettier"],

  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",


    "no-unused-vars": 1,

    "no-undef": 1,


    "prettier/prettier": [1, { "singleQuote": true, "semi": false }],


  },
};
