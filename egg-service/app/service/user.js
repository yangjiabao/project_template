const Service = require("egg").Service;

class UserService extends Service {
  // 获取用户
  async show(req) {
    const { ctx } = this;
    let data = await ctx.model.User.find()
      .skip((req.pageIndex - 1) * req.pageSize)
      .limit(Number(req.pageSize));
    let count = await ctx.model.User.countDocuments();
    return { data, count };
  }

  // 获取用户信息
  async getUserInfo() {
    const { ctx } = this;
    // 解码token
    let username = ctx.decode.username;
    let roles_id = ctx.decode.roles;

    let rolesDocs = await ctx.model.Roles.find({ _id: { $in: roles_id } });
    let userDocs = await ctx.model.User.findOne({ username }).lean();
    // 根据字段中对象的某个属性查找使用该方式
    let loginUserDocs = await ctx.model.LoginUser.findOne({
      "user.username": username,
    });
    if (!loginUserDocs) {
      await ctx.model.LoginUser.insertMany({
        user: userDocs,
        roles: rolesDocs,
      });
    } else {
      await ctx.model.LoginUser.updateOne(
        { "user.username": username },
        {
          user: userDocs,
          roles: rolesDocs,
        }
      );
    }
    userDocs.roles = rolesDocs;
    return userDocs;
  }

  // 创建用户
  async create(req) {
    const { ctx } = this;
    await ctx.model.User.insertMany(req);
  }

  // 更新用户
  async update(req) {
    const { ctx } = this;
    await ctx.model.User.updateOne({ _id: req._id }, req);
  }

  // 删除用户
  async delete(req) {
    const { ctx } = this;
    await ctx.model.RolesUser.deleteMany({ user_id: { $in: req._id } });
    await ctx.model.User.deleteMany({ _id: { $in: req._id } });
  }

  // 获取角色用户
  async getRolesUser(req) {
    const { ctx } = this;
    let user_id = await ctx.model.RolesUser.find({
      roles_id: req.roles_id,
    }).distinct("user_id");
    let data = await ctx.model.User.find({ _id: { $in: user_id } });
    return data;
  }

  // 获取该角色可添加用户
  async getExtraUser(req) {
    const { ctx } = this;
    let user_id = await ctx.model.RolesUser.find({
      roles_id: req.roles_id,
    }).distinct("user_id");
    let data = await ctx.model.User.find({ _id: { $nin: user_id } });
    return data;
  }

  // 添加角色用户
  async addRolesUser(req) {
    const { ctx } = this;

    // await ctx.model.RolesUser.insertMany({
    //   roles_id: req.roles_id,
    //   user_id: { $in: req.user_id },
    // });
    for (let item of req.user_id) {
      await ctx.model.RolesUser.insertMany({
        roles_id: req.roles_id,
        user_id: item,
      });
    }
  }

  // 删除角色用户
  async deleteRolesUser(req) {
    const { ctx } = this;

    await ctx.model.RolesUser.deleteMany({
      roles_id: req.roles_id,
      user_id: { $in: req.user_id },
    });
  }
}

module.exports = UserService;
