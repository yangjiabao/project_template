import request from '@/utils/request'

export function login(username, password, code, uuid) {
  return request({
    url: 'api/auth/login',
    method: 'post',
    data: {
      username,
      password,
      code,
      uuid,
    },
  })
}

export function getUserInfo() {
  return request({
    url: 'api/auth/getUserInfo',
    method: 'get',
  })
}

export function getVerifyCode() {
  return request({
    url: 'api/auth/code' + '?t=' + new Date().getTime(),
    method: 'get',
  })
}

export function logout() {
  return request({
    url: 'api/auth/logOut',
    method: 'get',
  })
}

export function scanLogin(data) {
  return request({
    url: '/api/Auth/QyWxLogin',
    method: 'post',
    data,
  })
}

export function companyLogin(data) {
  return request({
    url: '/api/Auth/CompanyLogin',
    method: 'post',
    data,
  })
}

export function updateCompany(data) {
  return request({
    url: '/api​/Company​/Update',
    method: 'post',
    data,
  })
}

export function register(data) {
  return request({
    url: '/api​/Company​/Update',
    method: 'post',
    data,
  })
}
