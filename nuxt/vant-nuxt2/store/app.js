import Vue from 'vue'

console.log(Vue)

export const state = () => ({
    list: [],
    token:'',
    userInfo:{}
  })
  
  export const mutations = {

    SET_TOKEN(state,token){

        state.token = token

    },


    SET_USER_INFO(state,params){

      state.userInfo = params

  },


    
    add(state, text) {
      state.list.push({
        text,
        done: false
      })
    },
    remove(state, { todo }) {
      state.list.splice(state.list.indexOf(todo), 1)
    },
    toggle(state, todo) {
      todo.done = !todo.done
    }
  }



  export const actions = {
    login({ commit },params) {


        return new Promise((resolve, reject) => {

            this.$api.login(params).then(res => {



                // console.log(res,'res')


                localStorage.setItem('token',res.data);

                commit('SET_TOKEN',res.data)

                // this.$router.push('/')


                resolve(res.data)


                


            }).catch(err => {

              reject(err)


                // console.log(err)
            })

          })
     
    },





    



    getUserInfo({ commit }) {


      return new Promise((resolve, reject) => {

          this.$api.getUserInfo().then(res => {





              commit('SET_USER_INFO',res.data)


              localStorage.setItem('userInfo',res.data);


              this.$router.push('/')


              resolve(res.data)


              


          }).catch(err => {

            reject(err)

          })

        })
   
  },


  }