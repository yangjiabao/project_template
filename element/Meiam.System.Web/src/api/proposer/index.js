import request from '@/utils/request'

// 协助书
export function uploadAssist(data) {
  return request({
    url: '/api/Assist/UploadFile',
    method: 'post',
    data
  })
}
    
export function addAssist(data) {
  return request({
    url: '/api/Assist/Add',
    method: 'post',
    data
  })
}

export function queryAssist(data) {
  return request({
    url: '/api/Assist/Query',
    method: 'post',
    data
  })
}

export function updateAssist(data) {
  return request({
    url: '/api/Assist/Update',
    method: 'post',
    data
  })
}

export function submitAssist(data) {
  return request({
    url: '/api/Assist/Submit',
    method: 'post',
    data
  })
}

export function getAssistModel(data) {
  return request({
    url: '/api/Assist/GetModel',
    method: 'post',
    data
  })
}

export function getUploadList(data) {
  return request({
    url: '/api/UploadFile/GetUploadListByRequireCode',
    method: 'post',
    data
  })
}

export function deleteAssist(data) {
  return request({
    url: '/api/Assist/Delete',
    method: 'post',
    data
  })
}

export function deleteUploadFile(data) {
  return request({
    url: '/api/Assist/DeleteFile',
    method: 'post',
    data
  })
}

// 物品表
export function queryProduct(data) {
  return request({
    url: '/api/Product/Query',
    method: 'post',
    data
  })
}
  
export function addProduct(data) {
  return request({
    url: '/api/Product/Add',
    method: 'post',
    data
  })
}
  
export function updateProduct(data) {
  return request({
    url: '/api/Product/Update',
    method: 'post',
    data
  })
}
  
export function deleteProduct(data) {
  return request({
    url: '/api/Product/Delete',
    method: 'post',
    data
  })
}

// 机动车表
export function queryCar(data) {
  return request({
    url: '/api/Car/Query',
    method: 'post',
    data
  })
}

export function addCar(data) {
  return request({
    url: '/api/Car/Add',
    method: 'post',
    data
  })
}

export function updateCar(data) {
  return request({
    url: '/api/Car/Update',
    method: 'post',
    data
  })
}

export function deleteCar(data) {
  return request({
    url: '/api/Car/Delete',
    method: 'post',
    data
  })
}

// 基础信息
export function getPriceConnotation() {
  return request({
    url: '/api/BaseData/GetPriceConnotation',
    method: 'get'
    
  })
}

export function getObjectCase() {
  return request({
    url: '/api/BaseData/GetObjectCase',
    method: 'get'
    
  })
}

export function getBuyStatues() {
  return request({
    url: '/api/BaseData/GetBuyStatues',
    method: 'get'
    
  })
}

export function getBasicDaySituationSure() {
  return request({
    url: '/api/BaseData/GetBasicDaySituationSure',
    method: 'get'
    
  })
}

export function getMeasure() {
  return request({
    url: '/api/BaseData/GetMeasure',
    method: 'get'
    
  })
}

export function getReplenish() {
  return request({
    url: '/api/BaseData/GetReplenish',
    method: 'get'
    
  })
}
  