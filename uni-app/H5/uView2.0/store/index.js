
import Vue from 'vue'
import Vuex from 'vuex'

import {login, getUserInfo} from '@/common/api.js'

Vue.use(Vuex) // vue的插件机制

// Vuex.Store 构造器选项
const store = new Vuex.Store({
  // 为了不和页面或组件的data中的造成混淆，state中的变量前面建议加上$符号
  state: {
    // 用户信息
    userInfo: {

    },
		
    token: ''
		
  },
	
  mutations: {
		
		    SET_TOKEN: (state, token) => {
		      state.token = token
		    },
			
			  SET_USER_INFO: (state, userInfo) => {
			      state.userInfo = userInfo
			    }
		
  },
  actions: {
		   // 登录
		    login({commit}, params) {
		      return new Promise((resolve, reject) => {
				  console.log(params, 'login')
		        login(
		        {
            phone: params.phone,
		         password: params.password
				  }
		          // params.code,
		          // params.uuid
		        )
		          .then((res) => {
					    console.log(res)
		            // if (res.statusCode === 200) {
						    uni.setStorageSync('token', res)
	
		              commit('SET_TOKEN', res)
		            // }
		            resolve(res)
		          })
		          .catch((err) => {
		            reject(err)
		          })
		      })
		    },
			
			    // 获取用户信息
			    getUserInfo({commit}) {
			      return new Promise((resolve, reject) => {
			        getUserInfo()
			          .then((res) => {
			            commit('SET_USER_INFO', res)
			
			            resolve(res)
			          })
			          .catch((error) => {
			            reject(error)
			          })
			      })
			    }
		
  }
	
})

export default store
