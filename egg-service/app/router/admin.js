"use strict";

module.exports = (app) => {
  const { router, controller } = app;

  router.get("/", controller.home.index);

  // 登录路由
  router.post("/login", controller.login.login);
  router.get("/getCode", controller.login.getCode);
  router.get("/logout", controller.login.logout);
  router.get("/routerMenu", controller.menu.getRouterMenu);

  // 上传
  router.post("/uploadSingle", controller.upload.uploadSingle);
  router.post("/uploadMultiple", controller.upload.uploadMultiple);
  router.post("/uploadVideo", controller.upload.uploadVideo);

  // 用户路由
  router.get("/userInfo", controller.user.getUserInfo);
  router.get("/user", controller.user.show);
  router.post("/user", controller.user.create);
  router.delete("/user", controller.user.delete);
  router.put("/user", controller.user.update);
  router.post("/uploadAvatar", controller.user.uploadAvatar);

  // 用户角色
  router.get("/rolesUser", controller.rolesUser.show);
  router.post("/rolesUser", controller.rolesUser.create);
  router.delete("/rolesUser", controller.rolesUser.delete);
  router.get("/extraUser", controller.rolesUser.getExtraUser);

  // 菜单路由
  router.get("/menu", controller.menu.show);
  router.post("/menu", controller.menu.create);
  router.delete("/menu", controller.menu.delete);
  router.put("/menu", controller.menu.update);

  // 角色路由
  router.get("/roles", controller.roles.show);
  router.post("/roles", controller.roles.create);
  router.put("/roles", controller.roles.update);
  router.delete("/roles", controller.roles.delete);

  // 字典路由
  router.get("/dictionary", controller.dictionary.show);
  router.post("/dictionary", controller.dictionary.create);
  router.put("/dictionary", controller.dictionary.update);
  router.delete("/dictionary", controller.dictionary.delete);

  // 权限路由
  router.get("/permission", controller.permission.show);
  router.post("/permission", controller.permission.create);
  router.put("/permission", controller.permission.update);
  router.delete("/permission", controller.permission.delete);

  // 角色权限

  router.get("/menuPermission", controller.rolesPermission.getMenuPermission);
  router.get("/rolesMenu", controller.menu.getRolesMenu);
  router.get("/rolesPermission", controller.rolesPermission.show);
  router.put("/rolesPermission", controller.rolesPermission.update);
};
