import Vue from 'vue'
import VueRouter from 'vue-router'

import config from '@/config'

import store from '@/store'

let whiteList = ['/login'] // no redirect whitelist

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    meta: {
      title: '登录',
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/login/index.vue'),
  },
  {
    path: '/home',
    name: 'Home',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/home/index.vue'),

    meta: {
      title: '首页',
      // 使用vant自带图标
      icon: 'wap-home-o',
      // 是否在tabbar中显示
      tabbar: true,
      // 自定义tabbar图标。图片路径为@/assets/images/
      // inactiveIcon: 'home.png',
      // activeIcon: 'home-active.png',
    },
  },

  {
    path: '/mine',
    name: 'Mine',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/mine/index.vue'),

    meta: {
      icon: 'contact',
      title: '我的',
      tabbar: true,
      // inactiveIcon: 'mine.png',
      // activeIcon: 'mine-active.png',
    },
  },

  { path: '/', redirect: '/home' },
]

const router = new VueRouter({
  routes,
})
// 路由守卫
router.beforeEach((to, from, next) => {
  console.log(to, from, next)

  // 页面标题
  document.title = to.meta.title ? to.meta.title : config.title

  // 登录路由守卫。如不需登录页面功能请注释掉
  if (whiteList.includes(to.path)) {
    next()
  } else {
    if (store.state.token) {
      if (JSON.stringify(store.state.userInfo) === '{}') {
        store
          .dispatch('getUserInfo')
          .then((res) => {
            next()
            console.log(res)
          })
          .catch((err) => {
            console.log(err)
            next(`/login?redirect=${to.path}`) // 否则全部重定向到登录页
          })
      } else {
        next()
      }
    } else {
      next(`/login?redirect=${to.path}`) // 否则全部重定向到登录页
    }
  }
})

export default router
