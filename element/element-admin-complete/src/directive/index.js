import downloadUrl from './downloadUrl'
import dragDialog from './dragDialog'
import permission from './permission'

export default (Vue) => {
  Vue.directive('download', downloadUrl)
  Vue.directive('drag-dialog', dragDialog)
  Vue.directive('permission', permission)
}
