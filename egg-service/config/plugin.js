"use strict";

/** @type Egg.EggPlugin */
module.exports = {
  validate: {
    enable: true,
    package: "egg-validate",
  },
  mongoose: {
    enable: true,
    package: "egg-mongoose",
  },
  jwt: {
    enable: true,
    package: "egg-jwt",
  },
  cors: {
    enable: true,
    package: "egg-cors",
  },

  validate: {
    enable: true,
    package: "egg-validate",
  },

  swaggerdoc: {
    enable: true, // 启用 swagger-ui 默认启用
    package: "egg-swagger-doc", // 指定 第三方插件 包名称
  },

  // bodyParser: {
  //   enable: true,

  // },

  // had enabled by egg
  // static: {
  //   enable: true,
  // }
};
