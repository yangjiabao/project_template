module.exports = {

  root: true,

  // 解析器选项。指定你想支持的语言，默认支持es5
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },

  // 指定代码运行环境
  env: {
    browser: true,
    node: true,
    es6: true,
  },

  
globals:{

  "BMapGL" : true
},


  // 使用第三方插件
  extends: ['plugin:vue/essential', 'eslint:recommended',"@vue/prettier"],



  // add your custom rules here
  //it is base on https://github.com/vuejs/eslint-config-vue
  rules: {

    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",


    "no-unused-vars":1,


    "prettier/prettier": [1, {"singleQuote": true,"semi": false}],

    "vue/no-unused-components":1, //禁止注册模板中未使用的组件

    // 'semi': [1, 'never'], //要求或禁止使用分号代替 ASI


  }
}