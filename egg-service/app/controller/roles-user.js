const Controller = require("egg").Controller;

class RolesUserController extends Controller {
  // 获取角色用户
  async show() {
    const { ctx, service } = this;
    let req = ctx.query;
    let res = await service.user.getRolesUser(req);
    ctx.helper.success({ ctx, res });
  }

  // 添加角色用户
  async create() {
    const { ctx, service } = this;
    await ctx.helper.authorize("rolesUser_create");

    let req = ctx.request.body;
    await service.user.addRolesUser(req);
    ctx.helper.success({ ctx });
  }

  // 删除角色用户
  async delete() {
    const { ctx, service } = this;
    await ctx.helper.authorize("rolesUser_delete");

    let req = ctx.request.body;
    await service.user.deleteRolesUser(req);
    ctx.helper.success({ ctx });
  }

  // 获取可以添加角色用户
  async getExtraUser() {
    const { ctx, service } = this;

    let req = ctx.query;
    let res = await service.user.getExtraUser(req);
    ctx.helper.success({ ctx, res });
  }
}
module.exports = RolesUserController;
