module.exports = (app) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const ArticleSchema = new Schema({
    title: {
      type: String,
    },

    summary: {
      type: String,
    },
    author: {
      type: String,
    },
    poster: {
      type: String,
    },
    status: {
      type: Boolean,
    },
    // sort: {
    //   type: Number,
    // },
    publish: {
      type: String,
    },

    picture: {
      type: Array,
    },

    video: {
      type: Array,
    },

    content: {
      type: String,
    },
    module: {
      // required: true,
      type: String,
    },

    page_id: {
      // required: true,
      type: String,
    },

    // 自动生成创建日期
    createTime: {
      type: Date,
      default: Date.now,
    },
  });

  // 第三个参数值为指定文档名。不知定需注意复数
  return mongoose.model("Article", ArticleSchema, "article");
};
