import { login, logout, scanLogin, getUserInfo } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/token'

const user = {
  state: {
    token: getToken(),
    userInfo: {},
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_USERINFO: (state, userInfo) => {
      state.userInfo = userInfo
    },
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        login(
          userInfo.username,
          userInfo.password,
          userInfo.code,
          userInfo.uuid
        )
          .then((res) => {
            if (res.statusCode === 200) {
              setToken(res.data)
              commit('SET_TOKEN', res.data)
            }
            resolve(res)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },

    // 扫码登录
    ScanLogin({ commit }, code) {
      return new Promise((resolve, reject) => {
        scanLogin({ code })
          .then((res) => {
            setToken(res.data)
            commit('SET_TOKEN', res.data)

            resolve(res)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },

    // 获取用户信息
    GetUserInfo({ commit }) {
      return new Promise((resolve, reject) => {
        getUserInfo()
          .then((res) => {
            commit('SET_USERINFO', res.data)

            resolve(res)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    // 登出
    LogOut({ commit }) {
      return new Promise((resolve, reject) => {
        logout()
          .then((res) => {
            commit('SET_TOKEN', '')
            commit('SET_USERINFO', {})
            removeToken()
            resolve(res)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
  },
}

export default user
