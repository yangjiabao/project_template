import Cookies from 'js-cookie'

let key = 'SYSTOKEN'

let expires = new Date(new Date().getTime() + 1 * 60 * 60 * 1000)

export function getToken() {
  return Cookies.get(key)
}

export function setToken(token) {
  return Cookies.set(key, token, { expires })
}

export function removeToken() {
  return Cookies.remove(key)
}
