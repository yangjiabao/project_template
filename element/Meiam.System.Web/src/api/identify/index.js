import request from '@/utils/request'
// 标的分类管理
export function getObjectCate() {
  return request({
    url: 'api/ObjectCate/GetObjectCate',
    method: 'get'

  })
}

export function addObjectCate(data) {
  return request({
    url: 'api/ObjectCate/AddObjectCate',
    method: 'post',
    data
  })
}

export function updateObjectCate(data) {
  return request({
    url: 'api/ObjectCate/UpdateObjectCate',
    method: 'post',
    data
  })
}

export function deleteObjectCate(data) {
  return request({
    url: 'api/ObjectCate/DeleteObjectCate',
    method: 'post',
    data
  })
}

export function getCountryObjectCate(params) {
  return request({
    url: 'api/CountryData/GetCountryObjectCate',
    method: 'get',
    params
  })
}

// 认定机构管理
export function queryIdentify(data) {
  return request({
    url: 'api/Identify/Query',
    method: 'post',
    data
  })
}

export function getIdentifyAll(data) {
  return request({
    url: '/api/Identify/QueryALl',
    method: 'post',
    data
  })
}

export function addIdentify(data) {
  return request({
    url: 'api/Identify/AddIdentify',
    method: 'post',
    data
  })
}

export function updateIdentify(data) {
  return request({
    url: 'api/Identify/UpdateIdentify',
    method: 'post',
    data
  })
}

export function deleteIdentify(data) {
  return request({
    url: 'api/Identify/DeleteIdentify',
    method: 'post',
    data
  })
}

export function uploadIdentify(data) {
  return request({
    url: 'api/Identify/IdentifyUpload',
    method: 'post',
    data
  })
}

export function uploadIdentifyEnable(data) {
  return request({
    url: 'api/Identify/UpdateIdentifyEnalbe',
    method: 'post',
    data
  })
}

export function getIdentifyByCode(params) {
  return request({
    url: '/api/Identify/GetFirstByCode',
    method: 'get',
    params
  })
}

export function getIdentifyOption(params) {
  return request({
    url: 'api/Identify/GetIdentifyOption',
    method: 'get',
    params
  })
}

// 计量单位
export function getMeasure(data) {
  return request({
    url: 'api/Measure/GetAll',
    method: 'post',
    data
  })
}

export function addMeasure(data) {
  return request({
    url: '/api/Measure/AddMeasure',
    method: 'post',
    data
  })
}

export function updateMeasure(data) {
  return request({
    url: '/api/Measure/UpdateMeasure',
    method: 'post',
    data
  })
}

export function deleteMeasure(data) {
  return request({
    url: '/api/Measure/DeleteMeasure',
    method: 'post',
    data
  })
}

// 区域管理

export function getDistrict(params) {
  return request({
    url: 'api/District/GetAllDistrict',
    method: 'get',
    params
  })
}

export function addDistrict(data) {
  return request({
    url: '/api/District/AddDistrict',
    method: 'post',
    data
  })
}

export function deleteDistrict(data) {
  return request({
    url: '/api/District/DeleteDistirct',
    method: 'post',
    data
  })
}

export function updateDistrict(data) {
  return request({
    url: '/api/District/UpdateDistrict',
    method: 'post',
    data
  })
}

// 案件性质管理
export function getCaseNature(data) {
  return request({
    url: '/api/CaseNature/GetAll',
    method: 'post',
    data
  })
}

export function addCaseNature(data) {
  return request({
    url: '/api/CaseNature/AddCaseNature',
    method: 'post',
    data
  })
}

export function updateCaseNature(data) {
  return request({
    url: '/api/CaseNature/UpdateCaseNature',
    method: 'post',
    data
  })
}

export function deleteCaseNature(data) {
  return request({
    url: '/api/CaseNature/DeleteCaseNature',
    method: 'post',
    data
  })
}

export function getCountryCaseNature(params) {
  return request({
    url: '/api/CountryData/GetCountryCaseNature',
    method: 'get',
    params
  })
}

// 认定模块数据
export function getLaw(data) {
  return request({
    url: '/api/Law/GetALL',
    method: 'post',
    data
  })
}

export function addLaw(data) {
  return request({
    url: '/api/Law/Add',
    method: 'post',
    data
  })
}

export function updateLaw(data) {
  return request({
    url: '/api/Law/Update',
    method: 'post',
    data
  })
}

export function deleteLaw(data) {
  return request({
    url: '/api/Law/Delete',
    method: 'post',
    data
  })
}

export function getLawEnable(data) {
  return request({
    url: '/api/Law/GetEnable',
    method: 'post',
    data
  })
}

// 价格内涵
export function getPriceConnote(params) {
  return request({
    url: '/api/PriceConnotation/GetAll',
    method: 'get',
    params
  })
}

export function addPriceConnote(data) {
  return request({
    url: '/api/PriceConnotation/AddPriceConnotation',
    method: 'post',
    data
  })
}

export function updatePriceConnote(data) {
  return request({
    url: '/api/PriceConnotation/UpdatePriceConnotation',
    method: 'post',
    data
  })
}

export function updatePriceConnoteEnable(data) {
  return request({
    url: '/api/PriceConnotation/UpdateEnable',
    method: 'post',
    data
  })
}

export function deletePriceConnote(data) {
  return request({
    url: '/api/PriceConnotation/DeletePriceConnotation',
    method: 'post',
    data
  })
}

// 结论书模板
export function getTemplateList(data) {
  return request({
    url: '/api/Temple/GetALl',
    method: 'post',
    data
  })
}

export function getTemplate(data) {
  return request({
    url: '/api/Temple/GetFirst',
    method: 'post',
    data
  })
}

export function addTemplate(data) {
  return request({
    url: '/api/Temple/Add',
    method: 'post',
    data
  })
}

export function updateTemplate(data) {
  return request({
    url: '/api/Temple/Update',
    method: 'post',
    data
  })
}

export function getPreviewTemplate(data) {
  return request({
    url: '/api/Temple/GetPreviewTemple',
    method: 'post',
    data
  })
}

export function deleteTemplate(data) {
  return request({
    url: '/api/Temple/Delete',
    method: 'post',
    data
  })
}

// 委托单位
export function queryOrganize(data) {
  return request({
    url: '/api/Organize/Query',
    method: 'post',
    data
  })
}

export function getOrganize(data) {
  return request({
    url: '/api/Organize/GetFirst',
    method: 'post',
    data
  })
}

export function addOrganize(data) {
  return request({
    url: '/api/Organize/Add',
    method: 'post',
    data
  })
}

export function updateOrganize(data) {
  return request({
    url: '/api/Organize/Update',
    method: 'post',
    data
  })
}

export function deleteOrganize(data) {
  return request({
    url: '/api/Organize/Delete',
    method: 'post',
    data
  })
}

// 流程管理

export function queryFlow(data) {
  return request({
    url: '/api/Flow/Query',
    method: 'post',
    data
  })
}

export function getFlow(id) {
  return request({
    url: '/api/Flow/Get?id=' + id,
    method: 'get'
  })
}

export function addFlow(data) {
  return request({
    url: '/api/Flow/AddFlow',
    method: 'post',
    data
  })
}

export function updateFlow(data) {
  return request({
    url: '/api/Flow/UpdateFlow',
    method: 'post',
    data
  })
}

export function deleteFlow(data) {
  return request({
    url: '/api/Flow/Delete',
    method: 'post',
    data
  })
}

export function addFlowLine(data) {
  return request({
    url: '/api/Flow/AddFlowLine',
    method: 'post',
    data
  })
}

export function getNodeList(id) {
  return request({
    url: '/api/Flow/GetNodeList?flowNo=' + id,
    method: 'post'

  })
}