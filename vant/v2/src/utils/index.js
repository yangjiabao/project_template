// 下载文件
export function downloadFile(file) {
  let a = ''
  a = document.createElement('a')
  a.style.display = 'none'
  let timeStamp = new Date().getTime()

  console.log(typeof file === 'string')
  // 判断传入为链接还是文件流
  if (typeof file === 'string') {
    let baseUrl = process.env.VUE_APP_BASE_API // 若是不完整的url则需要拼接baseURL
    let completeUrl = file.includes('http') ? file : baseUrl + file // 完整的url则直接使用

    // 这里是将url转成blob地址，
    fetch(completeUrl)
      .then((res) => res.blob())
      .then((blob) => {
        console.log(URL.createObjectURL(blob))

        // 将链接地址字符内容转变成blob地址
        a.href = URL.createObjectURL(blob)
        // 下载文件的名字
        a.download = `${timeStamp}_${
          completeUrl.split('/')[completeUrl.split('/').length - 1]
        }`

        document.body.appendChild(a)
        a.click()
        document.body.removeChild(a)
      })
  } else {
    a.href = URL.createObjectURL(new Blob([file]))
    a.download = `${timeStamp}_${file.name}`

    document.body.appendChild(a)
    a.click()
    document.body.removeChild(a)
  }
}

// 创建script标签引入外部js
export function loadScript(src) {
  return new Promise((resolve, reject) => {
    let script = document.querySelector(`script[src="${src}"]`)

    if (!script) {
      let script = document.createElement('script')

      script.type = 'text/javascript'

      script.src = src

      script.addEventListener('load', () => resolve(script))

      script.addEventListener('error', (event) => reject(event))

      script = document.head.appendChild(script)
    } else {
      resolve(script)
    }
  })
}
