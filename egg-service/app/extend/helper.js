module.exports = {
  success(param) {
    let { res = null, msg = "接口请求成功" } = param ? param : {};
    this.ctx.body = {
      code: 200,
      data: res,
      msg,
    };
  },

  fail(msg = "接口请求失败") {
    this.ctx.throw(402, msg);
  },

  async authorize(permission) {
    let { ctx, app } = this;

    let token = ctx.request.header.authorization;

    let username = app.jwt.verify(token, app.config.jwt.secret).username;

    let permissionDocs = await ctx.model.LoginUser.findOne({
      "user.username": username,
    }).distinct("permission.name");

    if (!permissionDocs.includes(permission)) {
      ctx.throw(407, "没有接口访问权限");
    }
  },
};
