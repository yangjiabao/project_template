const Service = require("egg").Service;

class ArticleService extends Service {
  // 获取字典
  async show(req) {
    const { ctx } = this;
    let data = await ctx.model.Article.find({ page_id: req.page_id });
    return data;
  }

  async detail(req) {
    const { ctx } = this;
    let data = await ctx.model.Article.findOne({ _id: req._id });
    return data;
  }

  async create(req) {
    const { ctx } = this;
    await ctx.model.Article.insertMany(req);
  }

  async delete(req) {
    const { ctx } = this;
    await ctx.model.Article.deleteOne({ _id: req._id });
  }

  async update(req) {
    const { ctx } = this;
    await ctx.model.Article.findByIdAndUpdate(req._id, req);
  }
}

module.exports = ArticleService;
