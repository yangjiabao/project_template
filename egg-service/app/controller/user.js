const Controller = require("egg").Controller;
const path = require("path");
const fs = require("fs");
const awaitWriteStream = require("await-stream-ready").write;
const sendToWormhole = require("stream-wormhole");

class UserController extends Controller {
  // 获取用户信息
  async getUserInfo() {
    const { ctx, service } = this;
    let res = await service.user.getUserInfo();
    ctx.helper.success({ res });
  }

  async uploadAvatar() {
    const { ctx } = this;
    const stream = await ctx.getFileStream();

    const filename = path.basename(stream.filename); // 文件名称
    let extname = path.extname(stream.filename).toLowerCase(); // 文件扩展名称

    if (extname !== ".jpg" && extname !== ".png") {
      ctx.helper.fail("请上传jpg/png格式图片");
    }

    let timestamp = new Date().getTime();

    let src = `/public/uploads/avatar/${timestamp}-${filename}`;

    const target = path.join(
      this.config.baseDir,
      "app/public/uploads/avatar",
      timestamp + "-" + filename
    );

    const writeStream = fs.createWriteStream(target);

    try {
      await awaitWriteStream(stream.pipe(writeStream));
    } catch (err) {
      // 必须将上传的文件流消费掉，要不然浏览器响应会卡死
      await sendToWormhole(stream);
      throw err;
    }
    ctx.helper.success({ res: src });
  }

  // 获取用户列表
  async show() {
    const { ctx, service } = this;
    // await ctx.helper.authorize("user_show");
    let req = ctx.query;
    let res = await service.user.show(req);
    ctx.helper.success({ res });
  }

  // 创建用户
  async create() {
    const { ctx, service } = this;
    await ctx.helper.authorize("userList_create");
    let req = ctx.request.body;
    await service.user.create(req);
    ctx.helper.success();
  }

  // 更新用户
  async update() {
    const { ctx, service } = this;
    await ctx.helper.authorize("userList_update");
    let req = ctx.request.body;
    await service.user.update(req);
    ctx.helper.success();
  }

  // 删除用户
  async delete() {
    const { ctx, service } = this;
    await ctx.helper.authorize("userList_delete");
    ctx.validate({ _id: "array" });
    let req = ctx.request.body;
    await service.user.delete(req);
    ctx.helper.success();
  }

  // 获取角色用户
  async getRolesUser() {
    const { ctx, service } = this;
    let req = ctx.query;
    let res = await service.user.getRolesUser(req);
    ctx.helper.success({ res });
  }
}
module.exports = UserController;
