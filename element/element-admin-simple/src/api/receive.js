import request from '@/utils/request'

export function queryReceive(data) {
  return request({
    url: '/api/Receive/viewReceive',
    method: 'post',
    data,
  })
}

export function updateReceive(data) {
  return request({
    url: '/api/Receive/update',
    method: 'post',
    data,
  })
}

export function deleteReceive(data) {
    return request({
      url: '/api/Receive/delete',
      method: 'delete',
      data,
    })
  }
  

