import router from './routers'
import store from '@/store'
import Config from '@/settings'

// 路由跳转进度条
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/token' // getToken from cookie

// 配置进度条不需要显示加载圈圈
NProgress.configure({ showSpinner: false }) // NProgress Configuration

// 白名单
const whiteList = ['/login'] // no redirect whitelist

router.beforeEach((to, from, next) => {
  NProgress.start()

  if (to.meta.title) {
    document.title = to.meta.title + ' - ' + Config.title
  }

  if (whiteList.includes(to.path)) {
    next()
  } else {
    if (getToken()) {
      if (JSON.stringify(store.getters.userInfo) === '{}') {
        // 判断当前用户是否已拉取完user_info信息

        store
          .dispatch('GetUserInfo')
          .then(() => {
            // 动态路由，拉取菜单
            store
              .dispatch('GenerateRoutes')
              .then(() => {
                next({ ...to, replace: true })
              })
              .catch(() => {
                next(`/login?redirect=${to.path}`) // 否则全部重定向到登录页
                // store.dispatch('LogOut').then(() => {
                //   location.reload() // 为了重新实例化vue-router对象 避免bug
                // })
              })
          })
          .catch(() => {
            next(`/login?redirect=${to.path}`) // 否则全部重定向到登录页
            // store.dispatch('LogOut').then(() => {
            //   location.reload() // 为了重新实例化vue-router对象 避免bug
            // })
          })
      } else {
        next()
      }
    } else {
      next(`/login?redirect=${to.path}`) // 否则全部重定向到登录页
    }
  }
})

router.afterEach(() => {
  NProgress.done() // finish progress bar
})
