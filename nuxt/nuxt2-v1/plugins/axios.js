import {Notification} from 'element-ui'

export default function({$axios, redirect}) {
//   $axios.onRequest((config) => {
//     console.log('Making request to ' + config.url)
//   })

  //   $axios.onError((error) => {
  //     const code = parseInt(error.response && error.response.status)
  //     if (code === 400) {
  //       redirect('/400')
  //     }
  //   })

  $axios.onResponse((response) => {
    if (response.data.statusCode !== 200) {
      Notification.error({
        title: '接口请求失败',
        message: response.data.message
      })
    }
  })
}
