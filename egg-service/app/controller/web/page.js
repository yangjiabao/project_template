const Controller = require("egg").Controller;

class PageController extends Controller {
  async show() {
    const { ctx, service } = this;
    let req = ctx.query;
    let res = await service.web.page.show(req);

    ctx.helper.success({ res });
  }

  async create() {
    const { ctx, service } = this;
    let req = ctx.request.body;
    await service.web.page.create(req);

    ctx.helper.success();
  }

  async delete() {
    const { ctx, service } = this;
    let req = ctx.request.body;
    await service.web.page.delete(req);

    ctx.helper.success();
  }

  async update() {
    const { ctx, service } = this;
    let req = ctx.request.body;
    await service.web.page.update(req);

    ctx.helper.success();
  }


}
module.exports = PageController;
