import { constantRouterMap } from '@/router/routers'
import Layout from '@/layout/index'

import router from '@/router/routers'

import { getUserMenus } from '@/api/system/menus'

const routers = {
  state: {
    routers: constantRouterMap,
    addRouters: [],
  },
  mutations: {
    SET_ROUTERS: (state, routers) => {
      state.addRouters = routers
      state.routers = constantRouterMap.concat(routers)
    },
  },
  actions: {
    GenerateRoutes({ commit }) {
      return new Promise((resolve, reject) => {
        getUserMenus()
          .then((res) => {
            let asyncRouter = filterAsyncRouter(res.data)
            asyncRouter.push({ path: '*', redirect: '/404', hidden: true })

            // 存储路由

            router.addRoutes(asyncRouter) // 动态添加可访问路由表

            commit('SET_ROUTERS', asyncRouter)

            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })

        // getMessage()
        //   .then((res) => {
        //     commit('GET_MESSAGE', res.data)
        //     resolve(res)
        //   })
        //   .catch((error) => {
        //     reject(error)
        //   })
      })
    },
  },
}

export const filterAsyncRouter = (routers) => {
  // 遍历后台传来的路由字符串，转换为组件对象
  return routers.filter((router) => {
    if (router.component) {
      if (router.component === 'Layout') {
        // Layout组件特殊处理
        router.component = Layout
      } else {
        const component = router.component
        router.component = loadView(component)
      }
    }
    if (router.children && router.children.length) {
      router.children = filterAsyncRouter(router.children)
    }
    return true
  })
}

export const loadView = (view) => {
  return (resolve) => require([`@/views/${view}`], resolve)
}

export default routers
