1、登录页面
2、底部导航栏
3、vuex
4、http请求拦截和响应拦截
6、API集中管理
7、自定义扩展图标库
8、uView组件库（V1）
9、公共样式库






Eslint配置

module.exports = {
	"extends": ["plugin:vue/essential", "eslint:recommended"],
	"parserOptions": {
		'ecmaVersion': 2018,
		'sourceType': 'module',
		'ecmaFeatures': {
			'jsx': true
		},
		'allowImportExportEverywhere': false
	},
	"env": {
		"browser": true,
		"node": true,
		"es6": true
	},
	"globals": {

		"uni": true
	},
	//规则
	"rules": {
		"no-alert": 0,
		"no-multi-spaces": 1, // 禁止多个空格 
		"semi": [1, "never"], // 自动补充分号
		"quotes": [1, "single"], // 使用单引号

		"vue/max-attributes-per-line": ["error", {
			"singleline": {
				"max": 1
			},
			"multiline": {
				"max": 1
			}
		}], //Enforce the maximum number of attributes per line

		"no-multiple-empty-lines": [1, {
			'max': 1
		}], //不允许多个空行

		"indent": [1, 2], //强制使用一致的缩进 (indent)

		"vue/html-indent": [1, 2, {
			"attribute": 2,

		}], //html中强制使用一致的缩进

		"no-unused-vars": 1, //禁止出现未使用过的变量

		"no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
		"no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",

	}
};
