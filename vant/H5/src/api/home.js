import request from '@/utils/request'

export function queryCategory(data) {
  return request({
    url: '/api/Column/viewQuery',
    method: 'post',
    data,
    toast: true,
  })
}

export function getUserInfo(params) {
  return request({
    url: '/api/Auth/GetUserInfo',
    method: 'get',
    params,
  })
}
