const Controller = require("egg").Controller;

class RolesPermissionController extends Controller {
  // 获取拥有菜单
  async getMenuPermission() {
    const { ctx, service } = this;
    let req = ctx.query;
    let res = await service.permission.getMenuPermission(req);

    ctx.helper.success({ res });
  }

  // 获取角色菜单权限
  async show() {
    const { ctx, service } = this;
    // await ctx.helper.authorize("rolesPermission_show");
    let req = ctx.query;
    let res = await service.permission.getRolesPermission(req);

    ctx.helper.success({ res });
  }

  // 更新角色菜单权限
  async update() {
    const { ctx, service } = this;
    await ctx.helper.authorize("rolesPermission_update");
    let req = ctx.request.body;
    await service.permission.updateRolesPermission(req);
    ctx.helper.success();
  }
}
module.exports = RolesPermissionController;
