module.exports = (app) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  const PageSchema = new Schema({
    title: {
      type: String,
    },

    content: {
      type: String,
    },
    icon: {
      type: String,
    },
    // article_id: {
    //   type: String,
    // },

    // 自动生成创建日期
    createTime: {
      type: Date,
      default: Date.now,
    },
  });

  // 第三个参数值为指定文档名。不知定需注意复数
  return mongoose.model("Page", PageSchema, "page");
};
