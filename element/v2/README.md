# Meiam.System.Web

Meiam.System 前端源码

#### 前端模板

初始模板基于： [https://github.com/elunez/eladmin-web](https://github.com/elunez/eladmin-web)

模板文档： [https://docs.auauz.net/#/](https://docs.auauz.net/#/)

#### Build Setup
``` bash
# 配置镜像加速
npm config set registry https://registry.npm.taobao.org

# 安装依赖
npm install

# 启动服务 localhost:18888
npm run dev

# 构建生产环境
npm run build

```

#### VSCODE 配置

请安装 ESLint & Vetur & Element UI Snippets & Vue 2 Snippets 插件







#### 工具库
1、wangeditor富文本  https://www.wangeditor.com/
2、tinymce富文本  http://tinymce.ax-z.cn/
3、dayjs日期处理  https://www.npmjs.com/package/dayjs
4、echarts图标库  https://echarts.apache.org/zh/index.html
5、element-ui ui库 https://element.eleme.cn/#/zh-CN
6、file-saver 文件处理  https://www.npmjs.com/package/file-saver
7、lodash 是一个一致性、模块化、高性能的 JavaScript 实用工具库  https://www.lodashjs.com/
8、js-cookie cookie处理库  https://www.npmjs.com/package/js-cookie
9、md5加密 https://www.npmjs.com/package/md5
10、normalize 样式重置   https://www.npmjs.com/package/normalize
11、vuedraggable 元素拖拽  https://www.npmjs.com/package/vuedraggable
12、xlsx处理  https://www.npmjs.com/package/xlsx
13、qs 一个查询字符串解析和字符串化库   https://github.com/ljharb/qs
14、sortablejs 功能强大的JavaScript 拖拽库  http://www.sortablejs.com/index.html#
15、nprogress 页面加载进度条  https://www.npmjs.com/package/nprogress
16、clipboard 粘贴板  https://www.npmjs.com/package/clipboard
