module.exports = (app) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const MenuSchema = new Schema({
    name: {
      unique: true,
      required: true,
      type: String,
    },
    icon: {
      type: String,
    },
    parent_id: {
      // 默认值
      default: "-1",
      type: String,
    },

    hidden: {
      type: Boolean,
    },
    redirect: {
      type: String,
    },
    component: {
      required: true,
      type: String,
    },
    sort: {
      type: Number,
      default: 1,
    },
    title: {
      type: String,
      required: true,
    },
  });

  return mongoose.model("Menu", MenuSchema, "menu");
};
