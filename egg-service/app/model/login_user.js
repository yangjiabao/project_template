module.exports = (app) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  const LoginUserSchema = new Schema({
    roles: {
      type: Array,
      required: true,
    },
    user: {
      type: Object,
      required: true,
    },
    permission: {
      type: Array,
    },

  });
  return mongoose.model("LoginUser", LoginUserSchema, "login_user");
};
