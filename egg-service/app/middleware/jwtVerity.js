module.exports = (options) => {
  return async function jwt(ctx, next) {
    const token = ctx.request.header.authorization;
    if (token) {
      try {
        // 解码token
        ctx.decode = ctx.app.jwt.verify(token, options.secret);
        await next();
      } catch (error) {
        if (error.name === "TokenExpiredError") {
          // ctx.throw(401, "TOKEN过期");
          ctx.body = {
            code: 401,
            msg: "TOKEN过期",
          };
        }
        if (error.name === "JsonWebTokenError") {
          // ctx.throw(401, error.message);
          ctx.body = {
            code: 401,
            msg: error.message,
          };
        }
      }
    } else {
      // ctx.throw(401, "没有TOKEN");
      ctx.body = {
        code: 401,
        msg: "没有TOKEN",
      };
    }
  };
};
