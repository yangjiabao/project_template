import request from '@/utils/request'

export function queryCategory(data) {
  return request({
    url: '/api/Column/viewQuery',
    method: 'post',
    data,
  })
}

export function createCategory(data) {
  return request({
    url: 'api/Column/Add',
    method: 'post',
    data,
  })
}

export function updateCategory(data) {
  return request({
    url: '​/api/Column/update',
    method: 'post',
    data,
  })
}

export function deleteCategory(data) {
  return request({
    url: '​/api/Column/delete',
    method: 'delete',
    data,
  })
}
