export default function printTable(el, title) {
  // 保存页面HTML
  // let originalHtml = document.body.innerHTML

  // 获取表格父容器元素
  let tableWrap = document.querySelector(el)

  // 设置标题
  document.title = title ? title : document.title

  let tables = tableWrap.querySelectorAll('.el-table')

  // 多个表格时循环处理
  for (let i = 0; i < tables.length; i++) {
    let tbody = tables[i].querySelector('.el-table__body-wrapper tbody')

    let thead = tables[i].querySelector('.el-table__header-wrapper thead')

    let footer = tables[i].querySelector('.el-table__footer-wrapper tbody')

    // 将第一个表格的表头插入到第二个表格中合为一个表格
    tables[i].querySelector('.el-table__body').insertBefore(thead, tbody)

    tables[i].querySelector('.el-table__body').setAttribute('border', '1')

    // 将第一个表格的表尾插入到第二个表格中合为一个表格
    if (footer) {
      tables[i].querySelector('.el-table__body').appendChild(footer)
    }
  }

  // 打印内容插入window中
  document.body.innerHTML = tableWrap.innerHTML

  // 调用打印
  window.print()

  // 恢复原始页面HTML
  // document.body.innerHTML = originalHtml

  // 打印完成重新加载页面
  window.location.reload()
}
