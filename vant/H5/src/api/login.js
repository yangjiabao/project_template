import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/api/Auth/VipLogin',
    method: 'post',
    data,
  })
}

export function loginOut(data) {
  return request({
    url: '/api/Auth/VipLogin',
    method: 'post',
    data,
  })
}

export function getUserInfo(params) {
  return request({
    url: '/api/Auth/GetUserInfo',
    method: 'get',
    params,
  })
}

export function getCode() {
  return request({
    url: 'api/auth/code',
    method: 'get',
  })
}
